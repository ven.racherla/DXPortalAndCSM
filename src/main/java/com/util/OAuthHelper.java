package com.util;

import com.auth0.jwt.algorithms.Algorithm;
import com.deciphex.library.encryption.main.Config;
import com.deciphex.library.encryption.main.WrappedHardwareToken;
import com.deciphex.library.encryption.main.WrappedKeyDerivation;
import com.deciphex.library.encryption.main.WrappedKeyManagement;
import com.deciphex.library.oauth.impl.OAuthGeneratorImpl;
import com.deciphex.library.oauth.model.OAuthConfig;
import com.deciphex.library.oauth.utils.OAuthUtil;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.log4j.Logger;
import javax.crypto.SecretKey;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class OAuthHelper {

    private static final Logger logger = LoggerHelper.getLogger(OAuthHelper.class);

    private final WrappedKeyManagement keyManagement = new WrappedKeyManagement(Config.defaults());
    private final WrappedKeyDerivation keyDerivation = new WrappedKeyDerivation(Config.defaults());

    private Algorithm algorithm = null;
    private String consumerKey = null;
    private PrivateKey corePrivateKey = null;

    public OAuthHelper() {

        KeyPair keyPair = keyManagement.generateAsymmetric();
        algorithm = Algorithm.RSA256((RSAPublicKey) keyPair.getPublic(), (RSAPrivateKey) keyPair.getPrivate());
        logger.info("---------------- GENERATE JWT PRIVATE/PUBLIC KEY ------------------");

        Path licenseFile = null;

        if (Files.exists(Paths.get("src/test/resources/license"))) {
            try (Stream<Path> fileStream = Files.list(Paths.get("src/test/resources/license"))) {
                List<Path> files = fileStream.filter(it -> it.getFileName().toString().endsWith(".key")).collect(Collectors.toList());
                if (files.size() == 1) {
                    licenseFile = files.get(0);
                } else {
                    logger.info("---------------- TOO MANY LICENCES ------------------");
                    System.exit(0);
                }
            } catch (IOException e) {
                logger.error("Unable to list license folder", e);
            }
        }
        if (licenseFile != null) {
            try {
                byte[] decryptedValue = getDecryptedBytes(licenseFile);
                Map<String, Object> keyMap = MapUtil.fromJson(new String(decryptedValue));
                consumerKey = keyMap.get("consumerKey").toString();
                byte[] content = Base64.decodeBase64(keyMap.get("privateKey").toString());
                corePrivateKey = keyManagement.loadPrivateKey(content);
            } catch (Exception e) {
                logger.info("---------------- INVALID LICENSE ------------------");
                System.exit(0);
            }
        } else {
            logger.info("---------------- LICENCE KEY NOT FOUND ------------------");
            System.exit(0);
        }
    }

    private byte[] getDecryptedBytes(Path licenseFile) throws IOException {
        Pair<?, ?> timeStampPair = keyDerivation.splitArray(Files.readAllBytes(licenseFile), 13);
        Pair<?, ?> keyPair = keyDerivation.splitArray((byte[]) timeStampPair.getValue(), 16);

        String fileNameWithoutExtension = FilenameUtils.removeExtension(licenseFile.getFileName().toString());
        if (fileNameWithoutExtension.contains("__")) {
            fileNameWithoutExtension = fileNameWithoutExtension.split("__")[1];
        }

        long createTimestampLong = Long.parseLong(new String((byte[]) timeStampPair.getKey()));
        //String resolvedMachineToken = config.machineToken.isEmpty() ? WrappedHardwareToken.getSerialNumber() : config.machineToken;
        String resolvedMachineToken = WrappedHardwareToken.getSerialNumber();

        logger.info("--- found license key: " + fileNameWithoutExtension + ", created: " + createTimestampLong);
        logger.info("--- found machine token: " + resolvedMachineToken);

        SecretKey secretKey = keyDerivation.deriveKey((byte[]) keyPair.getKey(), resolvedMachineToken, fileNameWithoutExtension, new Date(createTimestampLong));
        Pair<?, ?> valuePair = keyDerivation.splitArray((byte[]) keyPair.getValue(), 16);
        return keyManagement.decrypt(secretKey, (byte[]) valuePair.getKey(), (byte[]) valuePair.getValue());
    }

    public String getConsumerKey() {
        return consumerKey;
    }

    public PrivateKey getCorePrivateKey() {
        return corePrivateKey;
    }

    public OAuthGeneratorImpl oAuthGenerator() {
        OAuthGeneratorImpl oAuthGenerator = new OAuthGeneratorImpl(new OAuthConfig(OAuthUtil.SHA256), getCorePrivateKey(), getConsumerKey(), "");
        return oAuthGenerator;
    }
}
