package com.util;

import org.apache.log4j.Logger;
import org.apache.pdfbox.multipdf.Splitter;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;

import java.io.*;
import java.net.URL;
import java.util.Iterator;
import java.util.List;

public class PdfUtility {

    private static final Logger logger = LoggerHelper.getLogger(PdfUtility.class);


    public static String downloadFile(String fileURL, String fileName){

        String downloadFilePath = null;
        try (BufferedInputStream in = new BufferedInputStream(new URL(fileURL).openStream());
             FileOutputStream fileOutputStream = new FileOutputStream(fileName)) {
            byte dataBuffer[] = new byte[1024];
            int bytesRead;
            while ((bytesRead = in.read(dataBuffer, 0, 1024)) != -1) {
                fileOutputStream.write(dataBuffer, 0, bytesRead);
            }
        } catch (IOException e) {
            logger.info("An exception occured during file download " + e.getStackTrace());
        }

        return downloadFilePath;
    }


    public static void readPdf() throws IOException {

        //File pdfFile = new File("C:/PdfBox_Examples/patholytix_study_uploader_user_manual.pdf");
        InputStream is = new URL("http://localhost:8443/data/patholytix_study_uploader_user_manual.pdf").openStream();
        PDDocument document = PDDocument.load(is);

        Splitter splitter = new Splitter();
        List<PDDocument> Pages = splitter.split(document);

        Iterator<PDDocument> iterator = Pages.listIterator();

        int pdfFileSize = Pages.size();

        //PDDocument firstPage = Pages.get(0);


        PDFTextStripper reader = new PDFTextStripper();
        reader.setStartPage(1);
        reader.setEndPage(1);
        String pageText = reader.getText(document);

        logger.info("First page of user manual " + pageText);

    }
}
