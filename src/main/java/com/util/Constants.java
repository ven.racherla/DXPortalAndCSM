package com.util;

public final class Constants {
    public static final String CONFIG_FILE_PATH = "./src/test/resources/config/config.properties";
    public static final String CASE_UPLOADER_BASE_URL = "https://portal.dev.diagnexia.com/";
    public static final String UPLOAD_LIST_URL = "app/upload-list";
    public static final String HOME_PAGE_URL = "app/home";
    public static final String CASE_PAGE_URL = "app/case";
    public static final String STUDY_SOURCE_PATH = "c:\\Dechiphex_Hub\\StudySource\\";

    public static final String POWER_SHELL_SCRIPT_PATH = "./src/test/resources/scripts/%s";
    public static final String SCRIPT_SETUP_AWS_TEST_DATA ="setupTestDataFromS3ToStudySource.ps1";
    public static final String SCRIPT_DOWNLOAD_HUB_INSTALLER ="downloadPatholythixHubInstallerFromS3.ps1";
    public static final String SCRIPT_HUB_INSTALLER ="install/installhub.ps1";
    public static final String SCRIPT_CLEANUP ="install/cleanup.ps1";
    public static final String DXCASE_URL = "https://localhost:8443/login";
    public static final String reportConfigPath = "C:\\Users\\venkata.racherla\\DX_functional-tests\\src\\test\\resources\\extent-config.xml";
    }
