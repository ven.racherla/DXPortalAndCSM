package com.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Synchronized;

public class JsonUtil {
    private static JsonUtil instance;
    private final ObjectMapper objectMapper;

    protected JsonUtil() {
        this.objectMapper = new ObjectMapper();
    }

    @Synchronized
    public static ObjectMapper getInstance() {
        if (instance == null) {
            instance = new JsonUtil();
        }

        return instance.objectMapper;
    }
}
