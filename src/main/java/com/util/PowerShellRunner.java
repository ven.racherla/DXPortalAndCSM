package com.util;

import com.profesorfalken.jpowershell.PowerShell;
import com.profesorfalken.jpowershell.PowerShellNotAvailableException;
import com.profesorfalken.jpowershell.PowerShellResponse;
import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

public class PowerShellRunner {
    private static PowerShellRunner powerShellRunner = null;
    private static PowerShellResponse response = null;
    private static PowerShell powerShell;

    private static final Logger logger = LoggerHelper.getLogger(PowerShellRunner.class);

    private PowerShellRunner(){
        powerShell =  PowerShell.openSession();
    }

    public static PowerShellRunner getInstance(){
        if (powerShellRunner == null)
            powerShellRunner = new PowerShellRunner();

        return powerShellRunner;
    }

    public static PowerShellResponse RunPS1Script(String ps1ScriptPath, String timeoutMillis){

        try{

            //Increase timeout to give enough time to the script to finish
            Map<String, String> config = new HashMap<String, String>();
            config.put("maxWait", timeoutMillis);

            //Execute script
            response = powerShell.configuration(config).executeScript(ps1ScriptPath);

            //Print results if the script
            logger.info("Script output:" + response.getCommandOutput());

        } catch (PowerShellNotAvailableException ex)
        {
            logger.info("An error occured while running the script : " + ex.getStackTrace().toString());
        }

        return  response;
    }
}
