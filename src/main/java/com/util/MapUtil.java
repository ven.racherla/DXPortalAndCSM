package com.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

public class MapUtil {
    private final Map<String, Object> storage;

    private MapUtil() {
        this.storage = new LinkedHashMap<>();
    }

    public MapUtil put(String key, Object value) {
        this.storage.put(key, value);
        return this;
    }

    public MapUtil putAll(Map<String, Object> otherMap) {
        this.storage.putAll(otherMap);
        return this;
    }

    public Map<String, Object> build() {
        return this.storage;
    }

    public static MapUtil createInstance() {
        return new MapUtil();
    }

    public static Map<String, Object> fromJson(String jsonString) throws IOException {
        return JsonUtil.getInstance().readValue(jsonString, new TypeReference<>() {});
    }

    public static String toJson(Map<String, Object> jsonMap) throws JsonProcessingException {
        return JsonUtil.getInstance().writeValueAsString(jsonMap);
    }
}
