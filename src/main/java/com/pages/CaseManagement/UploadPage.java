package com.pages.CaseManagement;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.junit.Assert;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

public class UploadPage extends BasePage{

	private WebDriver driver;
	public UploadPage(WebDriver driver) {
		this.driver = driver;
	}

	private final String expectedHomePageTitleText = "Patholytix Hub - Upload Management -  Upload";

	@FindBy(id = "studyNameInput")
	private WebElement studyNameInput;

	@FindBy(id = "studyFileInput")
	private WebElement studyFileInput;

	@FindBy(id = "includeLabelSelect")
	private WebElement includeLabelSelect;

	@FindBy(id = "isGlpStudySelect")
	private WebElement isGlpStudySelect;

	@FindBy(id = "studyNameInput2")
	private WebElement studyNameInputUpdate;

	@FindBy(id = "studyFileInput2")
	private WebElement studyFileInputUpdate;

	@FindBy(id = "studyNameInput3")
	private WebElement studyNameInputExport;

	@FindBy(xpath = "//button[contains(@class,'btn btn-primary') and contains(text(),'Insert')]")
	private WebElement insertButton;

	@FindBy(xpath = "//button[contains(@class,'btn btn-primary') and contains(text(),'Update')]")
	private WebElement updateButton;

	@FindBy(xpath = "//button[contains(@class,'btn btn-primary') and contains(text(),'Export')]")
	private WebElement exportButton;

	@FindBy(id = "display-invalid-feedback-insert")
	private WebElement displayInvalidFeedbackInsertMessage;

	public void insertStudy(){
		insertButton.click();
	}

	public void updateStudy(){
	}

	public void exportStudy(){
	}

	public void insertStudyName(String studyName){

		studyNameInput.sendKeys(studyName);
	}

	public void selectStudyFileOptionFromDropDownList(String testData){

		Select select = new Select(studyFileInput);
		List<WebElement> allDropdownOptions = select.getOptions();
		Assert.assertTrue("The study file dropdown list is not populated", allDropdownOptions.size()>0);

		for (WebElement option : allDropdownOptions) {
			String currentStudyFileName = option.getText();
			if (currentStudyFileName.startsWith(testData+ "\\")) {
				select.selectByVisibleText(currentStudyFileName);
			}
		}
	}

	public void selectIncludeLabelFromDropDownList(String selection){

		Select select = new Select(includeLabelSelect);
		List<WebElement> allDropdownOptions = select.getOptions();
		Assert.assertTrue("The Include Label dropdown list is not populated", allDropdownOptions.size()>0);

		for (WebElement option : allDropdownOptions) {
			String includeLabelSelection = option.getText();
			if (includeLabelSelection.contains(selection)) {
				select.selectByVisibleText(includeLabelSelection);
			}
		}
	}

	public void selectGLPstudyTypeFromDropDownList(String selection){

		Select select = new Select(isGlpStudySelect);
		List<WebElement> allDropdownOptions = select.getOptions();
		Assert.assertTrue("The GLP study dropdown list is not populated", allDropdownOptions.size()>0);

		for (WebElement option : allDropdownOptions) {
			String isGLPstudySelection = option.getText();
			if (isGLPstudySelection.contains(selection)) {
				select.selectByVisibleText(isGLPstudySelection);
			}
		}
	}


	public void selectIncludeLabelObtionFromDropDownList(){

	}

	public void selectGLPstudyOptionFromDropDownList(){
	}

	public void submitInsertOfNewStudy(){
		insertButton.click();
	}

	public void submitExportStudy(){
		exportButton.click();
	}

	public void submitUpdateStudy(){
		updateButton.click();
	}

	public void clickOnStudyFileOptionsDropdown() {
		studyFileInput.click();
	}

	public void clickOnIncludeLabelDropdown(){
		includeLabelSelect.click();
	}

	public void clickGLPstudyDropdown(){
		isGlpStudySelect.click();
	}

	public void checkDataValidationMessage(String expectedDataValidationMessage) {
		Assert.assertTrue("Data validation message is not displayed ", displayInvalidFeedbackInsertMessage.isDisplayed());

		Assert.assertEquals("Data validation message is not equal to expected validation message", expectedDataValidationMessage, displayInvalidFeedbackInsertMessage.getText());
	}

	public String getStudyNameBasedOnStudyFileSelectDropdownList(String testData){
		selectStudyFileOptionFromDropDownList(testData);
		Select select = new Select(studyFileInput);
		String selectedStudyFileName = select.getFirstSelectedOption().getText();
		return  selectedStudyFileName.split("\\\\")[1].split("\\.")[0];

	}

}
