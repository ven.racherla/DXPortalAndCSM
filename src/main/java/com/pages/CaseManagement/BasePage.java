package com.pages.CaseManagement;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class BasePage {

    private WebDriver driver;

    public BasePage(WebDriver driver) {
        this.driver = driver;
    }

    public BasePage() {
    }

    @FindBy(css = ("#navbarSupportedContent"))
    protected WebElement username;

    @FindBy(xpath = ("//*[@id='upload-list-link6']/a"))
    protected WebElement addfilesactive;

    public String getUsernameText(){
        return username.getText();
    }

    public String getActiveContent(){
        return username.getText();
    }
    protected boolean isElementDisplayed(WebElement webElement){
        boolean isDisplayed = false;
        if(webElement.isDisplayed()) {
             isDisplayed = true;
        }
        return isDisplayed;
    }

    public String getCurrentPageTitle(){
        return driver.getTitle();
    }

    public String getCurrentPageURL(){
        return driver.getCurrentUrl();
    }

}
