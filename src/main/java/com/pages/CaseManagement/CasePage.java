package com.pages.CaseManagement;
import java.time.Duration;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CasePage extends BasePage{


    private WebDriver driver;
    public CasePage(WebDriver driver) {
        this.driver = driver;
    }

    @FindBy(id = "submit1")
    private WebElement SaveRole;

    @FindBy(xpath= "//*[@id=\"dataTable\"]/thead/tr/th[2]")
    private WebElement ClickStatus;

    @FindBy(xpath = ("//img[contains(@src,'/images/edit.png')]"))
    private WebElement EditPath;

    @FindBy(xpath = ("//*[@id=\"accordion\"]/div/div[2]/div/div[2]/div[1]/block/a/img"))
    private WebElement ClickAddFiles;

    @FindBy(xpath = "//*[@id=\"partialCaseForm\"]/a/button")
    private WebElement ClickCancel;

    @FindBy(xpath= "//*[@id=\"previewButton2\"]")
    private WebElement ClickAdd;





    public void clickStatus() {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(4, 1));
        ClickStatus.click();
        ClickStatus.click();

    }
    public void EditRole() {
        EditPath.click();
        // Create object of the Select class
        Select se = new Select(driver.findElement(By.xpath("//*[@id='editAssignedPathologistSelect']")));
       // Select the option with value "6"
        se.selectByValue("62164379e9e6e21b6a7e8076");
        // save role group
        SaveRole.click();
    }
    public  void Clickaddfiles() {
        ClickAddFiles.click();
    }
    public void ClickCancel(){
        ClickCancel.click();
    }
    public void ClickAddWithoutpopulatinginfo(){
        ClickAdd.click();

    }
    public void VerifyWarningMessage(){
        WebElement ele = driver.findElement(By.id("originalAccessionIdInput"));
        JavascriptExecutor js = (JavascriptExecutor) driver;
        String validationMessage = (String)js.executeScript("return arguments[0].validationMessage;", ele);
        validationMessage.equals("Please fill out this field.");

    }

}
