package com.pages.CaseManagement;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class PortalPage extends BasePage {


    private WebDriver driver;

    public PortalPage(WebDriver driver) {
        this.driver = driver;
    }

    @FindBy(id = "submit1")
    private WebElement SaveRole;

    @FindBy(id= "originalIdInput")
    private WebElement PatientOriginalAccessionID;

    @FindBy(xpath = "//*[@id=\"dataTable\"]/thead/tr/th[2]")
    private WebElement ClickStatus;

    @FindBy(xpath = "//*[@id=\"caseSummaryDropdown\"]/img")
    private WebElement ViewCaseSummary;

    @FindBy(xpath = "//*[@id=\"dataTable\"]/tbody/tr[1]/td[16]/div[2]/a/img")
    private WebElement ViewCaseFiles;

    @FindBy(xpath = "//*[@id=\"dataTable\"]/tbody/tr[1]/td[16]/div[3]/a/img")
    private WebElement EditCaseInfo;

    @FindBy(xpath = "//*[@id=\"dataTable\"]/tbody/tr[1]/td[16]/div[4]/a/img")
    private WebElement DeleteCaseInfo;

    @FindBy(xpath = "//*[@id=\"navbarSupportedContent\"]/ul/li[1]/a")
    private WebElement ClickHomeButton;


    @FindBy(xpath = "//button[text()='Pathologist')]")
    private WebElement ClickPathologistButton;

    @FindBy(xpath = "//button[text()='Patient'))]")
    private WebElement ClickPatientButton;

    @FindBy(xpath = "//button[text()='Event'))]")
    private WebElement ClickEventButton;

    @FindBy(xpath = "/html/body/div[2]/div/div/div/p/a")
    private  WebElement Addpathologist;

    @FindBy(xpath = "/html/body/div[2]/div[1]/div/div/p/a")
    private  WebElement Addpatient;

    @FindBy(xpath = "//*[@id=\"editPathLink\"]/img")
    private WebElement EditPath;

    @FindBy(xpath = "//*[@id=\"dataTable\"]/tbody/tr[1]/td[1]/a[1]/img")
    private WebElement DeletePatient;

    @FindBy(xpath = "//*[@id=\"dataTable\"]/tbody/tr[1]/td[1]/a[2]/img")
    private WebElement EditPatient;

    @FindBy(xpath= "//*[@id=\"dataTable\"]/tbody/tr[1]/td[2]/a/img")
    private WebElement EditRLMManager;

    @FindBy(xpath = "//*[@id='exportButton']")
    private WebElement ClickExport;

    @FindBy(xpath = "//*[@id='userId']")
    private WebElement ClickUserId;

    @FindBy(xpath ="//*[@id='startDate']")
    private WebElement ClickStartDate;

    @FindBy(xpath ="//*[@id='endDate']")
    private WebElement ClickendDate;

    @FindBy(xpath ="//*[@id='filterButton']")
    private WebElement ClickfilterButton;

    @FindBy(xpath = "//*[@id=\"headingTwo\"]/h3")
    private WebElement ExpandRefPanel;

    @FindBy(xpath= "//*[@id=\"headingThree\"]/h3")
    private WebElement ExpandDiagPanel;

    @FindBy(xpath= "//*[@id=\"headingFour\"]/h4")
    private WebElement ExpandPatientPanel;

//    @FindBy(xpath = "//button[contains(@class,'btn btn-outline-primary') and contains(text(),'Add referring institute')]")
    @FindBy(xpath ="/html/body/div[2]/div[1]/div/div/p/a")
    private WebElement AddRefInst;

    @FindBy(xpath = "//*[@id=\'dataTable\']/tbody/tr[1]/td[1]/a/img")
    private WebElement EditRefInst;

//    @FindBy(xpath = "//button[contains(@class,'btn btn-outline-primary') and contains(text(),'Add referring lab')]")
    @FindBy(xpath ="/html/body/div[2]/div[1]/div/div/p/a")
    private WebElement AddRefLab;

//    @FindBy(xpath = "//button[contains(@class,'btn btn-outline-primary') and contains(text(),'Add referring lab manager')]")
    @FindBy(xpath = "/html/body/div[2]/div[1]/div/div/p/a")
    private WebElement AddRefLabManager;

//    @FindBy(xpath = "//button[contains(@class,'btn btn-outline-primary') and contains(text(),'Add referring clinician')]")
     @FindBy(xpath = "/html/body/div[2]/div[1]/div/div/p/a")
    private WebElement Addrefclinician;

    @FindBy(xpath =  "//*[@id='dataTable']/tbody/tr[1]/td[1]/a[1]/img")
    private WebElement DeleteRCLab;

    @FindBy(xpath = "//*[@id=\"dataTable\"]/tbody/tr[1]/td[1]/a[2]/img")
    private WebElement EditRCLab;

    @FindBy(xpath = "//img[@title='View case summary']")
    private WebElement RefViewcasesummary;

    @FindBy(xpath = "//img[@title='View case files']")
    private WebElement RefViewcasefiles;

    @FindBy(xpath = "//img[@title='Edit case information']")
    private WebElement RefEditcaseinformation;

    @FindBy(xpath = "//img[contains(@class,'navbar-left img-fluid')]")
    private WebElement DaigImage;

    @FindBy(xpath ="//img[@title='Edit referring institute']")
    private WebElement Editreferringinstitute;

    @FindBy(xpath ="//img[@title='Edit referring lab']")
    private WebElement Editreflab;

    @FindBy(xpath ="//img[@title='Add referring clinician']")
    private WebElement Addtreflabclinician;

    @FindBy(xpath ="//img[@title='Delete referring clinician']")
    private WebElement DelrefClinician;

    @FindBy(xpath ="//img[@title='Edit referring clinician']")
    private WebElement Editrefclinician;

    @FindBy(xpath ="//img[@title='Edit assigned pathologist']")
    private WebElement EditAssignedPath;

    @FindBy(xpath = "//*[@id='collapseFour']/p/a[1]")
    private WebElement ClickAddPatient;

    @FindBy(xpath ="//img[@title='Edit patient']")
    private WebElement EditPatientInfo;

    @FindBy(id = "patientDOBInput")
    private WebElement PatientDOBInput;

    @FindBy(id="patientUpdateReasonForChangeInput")
    private WebElement patientUpdateReasonForChangeInput;





    public void clickStatus() {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(4, 1));
        ClickStatus.click();
        ClickStatus.click();

    }
    public void verifyCaseActions() {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(4, 1));
        ViewCaseSummary.isDisplayed();
        ViewCaseFiles.isDisplayed();
        EditCaseInfo.isDisplayed();
        DeleteCaseInfo.isDisplayed();

    }
    public void verifyRefCaseActions() {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(4, 1));
        RefViewcasesummary.isDisplayed();


    }
    public void clickHome(){
    ClickHomeButton.click();
    }
    public void ClickPathologist(){
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(4, 1));
        wait.until(ExpectedConditions.visibilityOfElementLocated(By
                .xpath("//button[text()='Pathologist']")));
        driver.findElement(By.xpath("//button[text()='Pathologist']")).click();
    }
    public void AddpathologistisPresent(){
        Addpathologist.isDisplayed();
    }
    public void ManagePath(){
        EditPath.isDisplayed();
    }
    public void ClickPatient(){
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(4, 1));
        wait.until(ExpectedConditions.visibilityOfElementLocated(By
                .xpath("//button[text()='Patient']")));
        driver.findElement(By.xpath("//button[text()='Patient']")).click();
    }
    public void AddPatientisPresent(){
        Addpatient.isDisplayed();
    }
    public void VerifyPatientActions(){
        DeletePatient.isDisplayed();
        EditPatient.isDisplayed();
    }
    public void ClickEvent(){
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(4, 1));
        wait.until(ExpectedConditions.visibilityOfElementLocated(By
                .xpath("//button[text()='Event']")));
        driver.findElement(By.xpath("//button[text()='Event']")).click();
    }
    public void VerifyEventActions(){
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(4, 1));
        // Select the option with value
        ClickUserId.sendKeys("venkata.racherla+manager@deciphex.com");
        //Set StartDate
        ClickStartDate.sendKeys("04/05/2022");
        // set End Date
        ClickendDate.sendKeys("06/05/2022");
        ClickfilterButton.click();
        ClickExport.click();

    }
    public void ExpandRefPanel(){
        ExpandRefPanel.click();
    }
    public void ExpandPatientPanel(){
        ExpandPatientPanel.click();
    }
    public void ClickRefInstitute(){
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(4, 1));
        wait.until(ExpectedConditions.visibilityOfElementLocated(By
                .xpath("//button[text()='Referring Institute']")));
        driver.findElement(By.xpath("//button[text()='Referring Institute']")).click();
    }
   public void VerifyRefInstActions(){
        AddRefInst.isEnabled();
        EditRefInst.isDisplayed();
   }
   public void ClickDelegatingReferringLab(){
       WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(4, 1));
       wait.until(ExpectedConditions.visibilityOfElementLocated(By
               .xpath("//button[text()='Delegating Referring Lab']")));
       driver.findElement(By.xpath("//button[text()='Delegating Referring Lab']")).click();
   }
   public void VerifyDelRefLabActions(){
        AddRefLab.isEnabled();
        EditRefInst.isDisplayed();
   }
   public void ClickReferringLabManager(){
       WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(4, 1));
       wait.until(ExpectedConditions.visibilityOfElementLocated(By
               .xpath("//button[text()='Referring Lab Manager']")));
       driver.findElement(By.xpath("//button[text()='Referring Lab Manager']")).click();
   }
   public void VerifyRLMActions(){
       AddRefLabManager.isEnabled();
       WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(4, 1));
       wait.until(ExpectedConditions.visibilityOfElementLocated(By
               .xpath("//*[@id=\"dataTable\"]/tbody/tr[1]/td[2]/a/img")));
       driver.findElement(By.xpath("//*[@id=\"dataTable\"]/tbody/tr[1]/td[2]/a/img")).isDisplayed();
   }
   public void ClickReferringClinician(){
       WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(4, 1));
       wait.until(ExpectedConditions.visibilityOfElementLocated(By
               .xpath("//button[text()='Referring Clinician']")));
       driver.findElement(By.xpath("//button[text()='Referring Clinician']")).click();
   }
   public void VerifyRCActions(){
       Addrefclinician.isEnabled();
       DeleteRCLab.isDisplayed();
       EditRCLab.isDisplayed();
   }
   public void ExpandDiagPanel(){
        ExpandDiagPanel.click();
   }
   public void clickClinicalDirector(){
       WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(4, 1));
       wait.until(ExpectedConditions.visibilityOfElementLocated(By
               .xpath("//button[text()='Clinical Director']")));
       driver.findElement(By.xpath("//button[text()='Clinical Director']")).click();
   }
    public void clickLabManager(){
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(4, 1));
        wait.until(ExpectedConditions.visibilityOfElementLocated(By
                .xpath("//button[text()='Lab Manager']")));
        driver.findElement(By.xpath("//button[text()='Lab Manager']")).click();
    }
    public void clickLab(){
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(4, 1));
        wait.until(ExpectedConditions.visibilityOfElementLocated(By
                .xpath("//button[text()='Lab']")));
        driver.findElement(By.xpath("//button[text()='Lab']")).click();
    }
    public void VerifyRLMRefInstActions(){
        DaigImage.isDisplayed();
        Editreferringinstitute.isDisplayed();

    }
    public void VerifyRLMRLabActions(){
        DaigImage.isDisplayed();
        Editreflab.isDisplayed();
    }
    public void VerifyRLMLabMgmtActions(){
        DaigImage.isDisplayed();
        String value = driver.findElement(By.xpath("//div[@class='col-lg-12 text-center']/h2")).getText();
        value.equals("Referring Lab Manager Management");
    }
    public void VerifyRefCMActions(){
        Addrefclinician.isDisplayed();
        DelrefClinician.isDisplayed();
        Editrefclinician.isDisplayed();
    }
    public void VerifyCDCaseActions(){
        ViewCaseSummary.isDisplayed();
        ViewCaseFiles.isDisplayed();
        EditAssignedPath.isDisplayed();

    }
    public void ClickAddPatientbutton(){
        //ClickAddPatient.click();
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5, 1));
        WebElement ele = driver.findElement(By.xpath("//*[@id='collapseFour']/p/a[1]"));
        JavascriptExecutor js = (JavascriptExecutor) driver;
        ele.click();
    }
    public void ViewPatient(){
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(4, 1));
        PatientOriginalAccessionID.isDisplayed();
        PatientDOBInput.isDisplayed();


    }
    public void ClickEditPatient(){
         EditPatientInfo.click();
    }
    public void EditPatientInformation(){
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(4, 1));
        String generatedString = RandomStringUtils.randomAlphanumeric(15);
        PatientOriginalAccessionID.sendKeys(generatedString);
        patientUpdateReasonForChangeInput.sendKeys("Blah");
        SaveRole.click();

    }
}
