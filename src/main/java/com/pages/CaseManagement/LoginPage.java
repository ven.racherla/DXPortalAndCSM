package com.pages.CaseManagement;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage extends BasePage {

	private WebDriver driver;

	public LoginPage(WebDriver driver) {
		this.driver = driver;
	}
	private static final String expectedLoginPageTitleText = "Clinical Director @ Diagnexia Portal (Dev) > Home";

	@FindBy(id = "inputEmail")
	private WebElement emailAddress;

	@FindBy(id = "inputPassword")
	private WebElement password;

	@FindBy(xpath = "//button[@class='btn btn-lg btn-outline-primary btn-block']")
	private WebElement signInButton;

	@FindBy(id= "logoutButtonLink")
	private WebElement Logout;

	@FindBy(xpath = "//div[@class='alert alert-danger']")
	private WebElement errorMessage;

	@FindBy(css = "div.mx-auto.w-35.text-center > h6")
	private WebElement loginFailedMessage;

	@FindBy(xpath = "//*[@src='/images/patholytix-logo.png']")
	private WebElement loginPagePatholythixPreclinicalLogoText;

	@FindBy(xpath = "//*[@class='mt-5 text-muted text-center']")
	private WebElement version;

	@FindBy(xpath = "//*[@class='mb-3 text-muted text-center']")
	private WebElement copyrightYears;

	public boolean isPatholythixPreclinicalLogoDisplayed(){
		return isElementDisplayed(loginPagePatholythixPreclinicalLogoText);
	}

	public String getCopyRightYearsIntervalText(){
		return copyrightYears.getText();
	}

	public String getPatholytixPreclinicalVersionText(){
		return version.getText();
	}

	public String getLoginPageTitle() {return driver.getTitle();}

	public void enterEmailAddress(String emailAdressValue) {emailAddress.sendKeys(emailAdressValue);}

	public void enterPassword(String passwordValue) {
		password.sendKeys(passwordValue);

	}

	public void clickOnLoginButton() {
		signInButton.click();
	}
	public void clickOnLogout() {
		Logout.click();
	}


	public void doLogin(String emailAdressValue, String passwordValue) {
		enterEmailAddress(emailAdressValue);
		enterPassword(passwordValue);
		clickOnLoginButton();
	}

	public String getLoginFailedErrorMessageText(){
		return errorMessage.getText();
	}

	public String getUserLoginFailedText(){
		return loginFailedMessage.getText();
	}

	public String getLoginPageExpectedTitleText(){return expectedLoginPageTitleText;}


	public void clickOnLogoutButton() {
		Logout.click();
	}
}
