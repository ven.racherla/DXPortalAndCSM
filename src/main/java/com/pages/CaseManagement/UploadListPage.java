package com.pages.CaseManagement;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class UploadListPage extends BasePage{

	private WebDriver driver;
	public UploadListPage(WebDriver driver) {
		this.driver = driver;
	}

	private final String expectedHomePageTitleText = "";

	@FindBy(id = "new-upload-link")
	private WebElement createNewUpload;

	public String getUploadListPageTitle() {return driver.getTitle();}

	public void clickCreateNewUpload(){
		createNewUpload.click();
	}

}
