package com.pages.CaseManagement;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class HomePage extends BasePage {

	private WebDriver driver;
	public HomePage(WebDriver driver) {
		this.driver = driver;
	}

	@FindBy(id = "upload-list-link")
	private WebElement uploadOption;

	@FindBy(xpath = "//*[contains(text(),'User Manual')]")
	private WebElement userManualButton;

	@FindBy(xpath = "//*[contains(text(),'Add Case')]")
	private WebElement AddCase;

	@FindBy(xpath = "//button[text()='Case'))]")
	private WebElement ClickCaseButton;

	@FindBy(xpath = "//button[text()='Pathologist')]")
	private WebElement ClickPathologistButton;

	@FindBy(xpath = "//button[text()='Patient'))]")
	private WebElement ClickPatientButton;

	@FindBy(xpath = "//button[text()='Event'))]")
	private WebElement ClickEventButton;

	@FindBy(id = "originalAccessionIdInput")
	private WebElement originalAccessionId;

	@FindBy(id = "descriptionOfMaterialsInput")
	private WebElement descriptionOfMaterials;

	@FindBy(id = "previewButton2")
	private WebElement AddButton;

	@FindBy(id = "refreshClinicianListIcon")
	private WebElement refreshClinicianListIcon;

	@FindBy(xpath = "//*[@id=\"headingTwo\"]")
	private WebElement headingTwo;

	@FindBy(xpath = "//*[@id=\"headingThree\"]")
	private WebElement headingThree;

	@FindBy(xpath = "//*[@id=\"headingFour\"]")
	private WebElement headingFour;

	@FindBy(xpath = "//*[@id=\"headingFive\"]")
	private WebElement headingFive;

	@FindBy(id = "referringClinicianInput")
	private WebElement referclinician;


	public void clickOnUploadOption() {
		uploadOption.click();
	}

	public void clickUserManualButton() {
		userManualButton.click();
	}
	public void ClickCaseButton() {
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(4, 1));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.xpath("//button[text()='Case']")));
		driver.findElement(By.xpath("//button[text()='Case']")).click();
	}

	public String getHomePageTitle() {return driver.getTitle();}
	public void ClickAddCaseButton(){AddCase.click();}
	public void ClickCancelAddCaseButton(){AddCase.click();}
	public void AddCaseInformation(){
		String generatedString = RandomStringUtils.randomAlphanumeric(15);
		originalAccessionId.sendKeys(generatedString);
		Select se = new Select(driver.findElement(By.xpath("//*[@id='caseSubSpecialitiesInput']")));
		// Select the option with value "9"
		se.selectByValue("GI");
		//Set Priority
		Select setPriority = new Select(driver.findElement(By.xpath("//*[@id='casePriorityInput']")));
		// Select the option with value "Urgent"
		setPriority.selectByValue("URGENT");
		// set type
		Select setType = new Select(driver.findElement(By.xpath("//*[@id='caseTypeInput']")));
		// Select the option with value "PRIMARY"
		setType.selectByValue("PRIMARY");
		descriptionOfMaterials.sendKeys("Blah Blah Blah");
		headingThree.click();
		//add referring clinician
		refreshClinicianListIcon.click();
		WebDriverWait RC = new WebDriverWait(driver, Duration.ofSeconds(3, 1));
		RC.until(ExpectedConditions.visibilityOfElementLocated(By
				.xpath("//*[@id='referringClinicianInput']")));
		driver.findElement(By.xpath("//*[@id='referringClinicianInput']")).sendKeys("Dr. Venkata Racherla (Ref. Clinician)");
		headingFour.click();
		//add patient
		WebDriverWait patient = new WebDriverWait(driver, Duration.ofSeconds(3, 1));
		patient.until(ExpectedConditions.visibilityOfElementLocated(By
				.xpath("//*[@id='patientInput']")));
		driver.findElement(By.xpath("//*[@id='patientInput']")).sendKeys("mr veee nom 11 Apr 1978");
		headingFive.click();
		//add ref lab
		WebDriverWait labinfo = new WebDriverWait(driver, Duration.ofSeconds(3, 1));
		labinfo.until(ExpectedConditions.visibilityOfElementLocated(By
				.xpath("//*[@id='labInput']")));
		driver.findElement(By.xpath("//*[@id='labInput']")).sendKeys("venkata.racherla+lab@deciphex.com");
		//add del ref lab
		WebDriverWait reflabinfo = new WebDriverWait(driver, Duration.ofSeconds(3, 1));
		reflabinfo.until(ExpectedConditions.visibilityOfElementLocated(By
				.xpath("//*[@id='referringLabInput']")));
		driver.findElement(By.xpath("//*[@id='referringLabInput']")).sendKeys("venkata.racherla+ref+lab@deciphex.com");
		AddButton.click();

	}





}
