package com.api;

public class Routes {

    public static String BASE_URI  ="https://develop.api.deciphex.net";
    public static String GET_STUDIES ="/api/study";
    public static String DELETE_STUDY ="/api/study/%s";

}
