package com.api;

import com.deciphex.library.oauth.exception.OAuthException;
import com.util.OAuthHelper;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.http.ContentType;
import io.restassured.http.Header;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.QueryableRequestSpecification;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.SpecificationQuerier;
import org.apache.http.HttpHeaders;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static io.restassured.RestAssured.given;

public class StudyAPI {

    private OAuthHelper oAuthHelper = new OAuthHelper();

    public Response getStudies() throws OAuthException {

        RequestSpecification requestSpecification = getBaseRequestSpecification(Routes.GET_STUDIES).build();
        QueryableRequestSpecification queryableRequestSpecification = SpecificationQuerier.query(requestSpecification);

        //sign the request
        List<Header> headers = queryableRequestSpecification.getHeaders().asList();
        String oauthAuthorisation = oAuthHelper.oAuthGenerator().generateSignature(Method.GET.toString(), queryableRequestSpecification.getURI().toString(), queryableRequestSpecification.getBody().toString().getBytes(), getMapFormattedRequestHeaders(headers));

        return RestAssured.
                given().
                spec(requestSpecification.header(HttpHeaders.AUTHORIZATION, oauthAuthorisation)).
                get();
    }

    public void deleteStudy(java.lang.String studyId) throws OAuthException {

        RequestSpecification requestSpecification = getBaseRequestSpecification(String.format(Routes.DELETE_STUDY,studyId)).build();
        QueryableRequestSpecification queryableRequestSpecification = SpecificationQuerier.query(requestSpecification);

        //sign the request
        List<Header> headers = queryableRequestSpecification.getHeaders().asList();
        String oauthAuthorisation = oAuthHelper.oAuthGenerator().generateSignature(Method.DELETE.toString(), queryableRequestSpecification.getURI().toString(), queryableRequestSpecification.getBody().toString().getBytes(), getMapFormattedRequestHeaders(headers));

           RestAssured.
                given().
                spec(requestSpecification.header(HttpHeaders.AUTHORIZATION, oauthAuthorisation)).
                delete().
                then().
                statusCode(204);
    }

    private Map<String, String> getMapFormattedRequestHeaders(List<Header> headers) {
        return headers.stream()
                .map(s -> s.toString().split("="))
                .collect(Collectors.toMap(a -> a[0], a -> a.length > 1 ? a[1] : ""));
    }

    private RequestSpecBuilder getBaseRequestSpecification(String endpoint) {

        return new RequestSpecBuilder()
                .setBaseUri(Routes.BASE_URI)
                .setBody("")
                .setContentType(ContentType.JSON)
                .setAccept(ContentType.JSON)
                .setBasePath(endpoint)
                .log(LogDetail.ALL);
    }
}
