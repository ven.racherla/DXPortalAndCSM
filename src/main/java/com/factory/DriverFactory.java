package com.factory;

import com.util.LoggerHelper;
import com.util.PowerShellRunner;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.rmi.Remote;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class DriverFactory {

	private static final Logger logger = LoggerHelper.getLogger(DriverFactory.class);

	public WebDriver driver;

	public static ThreadLocal<WebDriver> tlDriver = new ThreadLocal<>();

	/**
	 * This method is used to initialize the thradlocal driver on the basis of given
	 * browser
	 * 
	 * @param browser
	 * @return this will return tldriver.
	 */
	public WebDriver init_driver(String browser) throws MalformedURLException {

		logger.info("browser value is: " + browser);

		if (browser.equals("chrome")) {
			WebDriverManager.chromedriver().setup();

			ChromeOptions options = new ChromeOptions();
			options.setAcceptInsecureCerts(true);
			options.setHeadless(false);

			Map<String,Object> prefs = new HashMap<String, Object>();
			prefs.put("profile.default_content_settings.popups", 0);
			prefs.put("download.default_directory","src/target/test-output/downloads");
			prefs.put("plugins.plugins_disabled", new String[] {
					"Chrome PDF Viewer"
			});

			options.setExperimentalOption("prefs", prefs);
//			DesiredCapabilities capabilities = DesiredCapabilities.chrome();
//			capabilities.setCapability(ChromeOptions.CAPABILITY, options);

			tlDriver.set(new ChromeDriver(options));

		} else if (browser.equals("firefox")) {
			WebDriverManager.firefoxdriver().setup();
			tlDriver.set(new FirefoxDriver());
		} else if (browser.equals("safari")) {
			tlDriver.set(new SafariDriver());
		}
			else if (browser.equals("remote")) {

				DesiredCapabilities capabilities = new DesiredCapabilities();
				capabilities.setCapability("browserName", "chrome");
				capabilities.setCapability("browserVersion", "102.0");
				capabilities.setCapability("selenoid:options", Map.<String, Object>of(
						"enableVNC", true,
						"enableVideo", true
				));

			    tlDriver.set(new RemoteWebDriver(new URL("http://127.0.0.1:4444/wd/hub"), capabilities	));

				ChromeOptions options = new ChromeOptions();
			    options.setCapability("browserName", "chrome");
				options.setCapability("browserVersion", "102.0");
			    options.setCapability("selenoid:options", Map.<String, Object>of(
						"enableVNC", true,
						"enableVideo", false
				));
				tlDriver.set(new RemoteWebDriver(new URL("http://127.0.0.1:4444/wd/hub"), options));
		} else {
			logger.info("Please pass the correct browser value: " + browser);
		}

		getDriver().manage().deleteAllCookies();
		getDriver().manage().window().maximize();
		return getDriver();

	}

	/**
	 * this is used to get the driver with ThreadLocal
	 * 
	 * @return
	 */
	public static synchronized WebDriver getDriver() {
		return tlDriver.get();
	}
}
