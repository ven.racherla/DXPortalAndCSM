package stepDefinitions.StudyUploader;

import com.factory.DriverFactory;
import com.util.Constants;
import com.util.LoggerHelper;
import com.util.PdfUtility;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.apache.log4j.Logger;
import org.junit.Assert;

import java.io.IOException;

public class HomePageSteps extends BaseTest {

	private static final Logger logger = LoggerHelper.getLogger(HomePageSteps.class);

	String pdfContent;

	@When("user clicks on Upload option from home page")
	public void userClicksOnUploadOptionFromHomepage() {
		homePage.clickOnUploadOption();
	}

	@Then("user gets the title of the home page")
	public void userGetsTheTitleOfTheHomePage() {
		title = homePage.getHomePageTitle();
		logger.info("Home page title is: " + title);
	}

	@Then("page title should be {string}")
	public void page_title_should_be(String expectedTitleName) {
		Assert.assertTrue(title.equals(expectedTitleName));
	}

	@And("username displayed on home page is {string}")
	public void usernameDisplayedOnHomepageIs(String expectedUsername) {
		String readUsername = homePage.getUsernameText();
		Assert.assertEquals("Read username on the homepage doesn't match expected username", readUsername, expectedUsername);
	}

	@Given("user goes to home page")
	public void userGoesToUploadListPage() {
		DriverFactory.getDriver()
				.get(Constants.CASE_UPLOADER_BASE_URL.concat(Constants.HOME_PAGE_URL));
	}

	@When("user clicks on User Manual")
	public void userClicksOnUserManual() {
		homePage.clickUserManualButton();
	}

	@When("user clicks on Case Button")
	public void userClicksOnCaseButton() {
		homePage.ClickCaseButton();
	}

	@When("user opens PDF file")
	public void userOpensPDFfile() throws IOException {
		PdfUtility.readPdf();
	}


	@Then("page URL contains {string}")
	public void pageURLcontains(String value) throws IOException {
		Assert.assertTrue("Page URL doesn't contain " + value, basePage.getCurrentPageURL().contains(value));
		PdfUtility.downloadFile(basePage.getCurrentPageURL(), "app/case");
	}
	@Then("user assign a case to Pathologist")
	public void userAssignACaseToPathologist() { casePage.EditRole(); }

	@When("user select status to unassigned")
	public void userSelectStatusToUnassigned() {casePage.clickStatus(); }

	@When("user clicks on add files option from case uploader page.")
	public void userClicksOnAddFilesOptionFromCaseUploaderPage() {
		casePage.Clickaddfiles();
	}

	@And("Content displayed on case page is {string}")
	public void contentDisplayedOnCasePageIsExpectedActiveContent(String expectedContentsName) {
		String readUsername = homePage.getActiveContent();
		Assert.assertEquals("Read username on the case page doesn't match expected content", readUsername, expectedContentsName);
	}

	@When("user select add case button")
	public void userSelectAddCaseButton() {
		homePage.ClickAddCaseButton();
	}

	@Then("user submits case information")
	public void userSubmitsCaseInformation() {
		homePage.AddCaseInformation();
	}
}




