package stepDefinitions.StudyUploader;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.GherkinKeyword;
import com.aventstack.extentreports.gherkin.model.Feature;
import com.aventstack.extentreports.gherkin.model.Scenario;
import com.aventstack.extentreports.model.Log;
import org.jboss.aerogear.security.otp.Totp;
import com.deciphex.library.oauth.exception.OAuthException;
import com.factory.DriverFactory;
import com.util.ConfigReader;
import com.util.Constants;
import com.util.LoggerHelper;
//import com.util.OAuthHelper;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;

public class LoginPageSteps extends BaseTest {

	private static final Logger logger = LoggerHelper.getLogger(LoginPageSteps.class);

	private static String title;

	public LoginPageSteps() throws ClassNotFoundException {
	}

	@Given("user is on login page")
	public void user_is_on_login_page() throws OAuthException {

		DriverFactory.getDriver()
				.get(Constants.CASE_UPLOADER_BASE_URL);
	}

	public void user_is_on_CaseUploader_page() throws OAuthException {

		DriverFactory.getDriver()
				.get(Constants.DXCASE_URL);
	}

	@When("user gets the title of the login page")
	public void user_gets_the_title_of_the_login_page() {
		title = loginPage.getLoginPageTitle();
		logger.info("Login page title is: " + title);
	}

	@When("user enters username {string}")
	public void user_enters_username(String username) {
		loginPage.enterEmailAddress(username);
	}

	@When("user enters password {string}")
	public void user_enters_password(String password) {
		loginPage.enterPassword(password);
	}

	@When("user clicks on Login button")
	public void user_clicks_on_login_button() {
		loginPage.clickOnLoginButton();
	}

	@Then("Error message is displayed with")
	public void errorMessageWillBeDisplayedWith(String errorMessage) {
		Assert.assertEquals("Wrong or No error message is displayed : ", errorMessage, loginPage.getLoginFailedErrorMessageText());
	}

	@Then("{string} is displayed")
	public void userLoginFailedIsDisplayed(String message) {
		Assert.assertEquals("Wrong or No user login failed message is displayed : ", message, loginPage.getUserLoginFailedText());
	}

	@Given("user is logged in with user {string}")
	public void userIsLoggedInWithUser(String user) throws OAuthException {
		configReader = new ConfigReader();
		prop = configReader.getProperties(Constants.CONFIG_FILE_PATH);

		user_is_on_login_page();
		loginPage.doLogin(user, prop.getProperty("password2"));
		String otpKeyStr = "7VIQR3ME4MELL2YM"; // <- this 2FA secret key.
		Totp totp = new Totp(otpKeyStr);
		String twoFactorCode = totp.now();// <- got 2FA coed at this time!
		DriverFactory.getDriver().findElement(By.id("secretCode")).sendKeys(twoFactorCode);
		loginPage.clickOnLoginButton();
		title = loginPage.getLoginPageTitle();
		login_page_title_should_be(loginPage.getLoginPageExpectedTitleText());
	}

	@Given("user is logged in with user1 {string}")
	public void userIsLoggedInWithUser1(String user1) throws OAuthException {
		configReader = new ConfigReader();
		prop = configReader.getProperties(Constants.CONFIG_FILE_PATH);
		user_is_on_login_page();
		loginPage.doLogin(user1, prop.getProperty("password1"));
		String otpKeyStr = "ULHOQMWHFDRMMXD6"; // <- this 2FA secret key.
		Totp totp = new Totp(otpKeyStr);
		String twoFactorCode = totp.now();// <- got 2FA coed at this time!
		DriverFactory.getDriver().findElement(By.id("secretCode")).sendKeys(twoFactorCode);
		loginPage.clickOnLoginButton();

	}

	@Given("user is logged in with user2 {string}")
	public void userIsLoggedInWithUser2(String user2) throws OAuthException {
		configReader = new ConfigReader();
		prop = configReader.getProperties(Constants.CONFIG_FILE_PATH);
		user_is_on_login_page();
		loginPage.doLogin(user2, prop.getProperty("password"));
		String otpKeyStr = "CPLGIHNGACUSOV2W"; // <- this 2FA secret key.
		Totp totp = new Totp(otpKeyStr);
		String twoFactorCode = totp.now();// <- got 2FA coed at this time!
		DriverFactory.getDriver().findElement(By.id("secretCode")).sendKeys(twoFactorCode);
		loginPage.clickOnLoginButton();

	}
	@Given("user is logged in with user3 {string}")
	public void userIsLoggedInWithUser3(String user3) throws OAuthException {
		configReader = new ConfigReader();
		prop = configReader.getProperties(Constants.CONFIG_FILE_PATH);
		user_is_on_login_page();
		loginPage.doLogin(user3, prop.getProperty("password"));
		String otpKeyStr = "GANPVH3T26RPZA2V"; // <- this 2FA secret key.
		Totp totp = new Totp(otpKeyStr);
		String twoFactorCode = totp.now();// <- got 2FA coed at this time!
		DriverFactory.getDriver().findElement(By.id("secretCode")).sendKeys(twoFactorCode);
		loginPage.clickOnLoginButton();

	}
	@Given("user is logged in with user4 {string}")
	public void userIsLoggedInWithUser4(String user4) throws OAuthException {
		configReader = new ConfigReader();
		prop = configReader.getProperties(Constants.CONFIG_FILE_PATH);
		user_is_on_login_page();
		loginPage.doLogin(user4, prop.getProperty("password1"));
		String otpKeyStr = "Q36EZ2D7K5YOXSKM"; // <- this 2FA secret key.
		Totp totp = new Totp(otpKeyStr);
		String twoFactorCode = totp.now();// <- got 2FA coed at this time!
		DriverFactory.getDriver().findElement(By.id("secretCode")).sendKeys(twoFactorCode);
		loginPage.clickOnLoginButton();

	}


	@When("user logs in with user {string}")
	public void userLogsInWithUsernameAndPassword(String user) {
		configReader = new ConfigReader();
		prop = configReader.getProperties(Constants.CONFIG_FILE_PATH);

		loginPage.doLogin(user, prop.getProperty("password"));

		title = loginPage.getLoginPageTitle();
		login_page_title_should_be(loginPage.getLoginPageExpectedTitleText());
		usernameDisplayedOnHomepageIs(user);
	}

	@And("username displayed on login page is {string}")
	public void usernameDisplayedOnHomepageIs(String expectedUsername) {
		String readUsername = loginPage.getUsernameText();
		Assert.assertEquals("Read username on the login doesn't match expected username", readUsername, expectedUsername);
	}

	@Then("login page title should be {string}")
	public void login_page_title_should_be(String expectedTitleName) {
		Assert.assertEquals(title, expectedTitleName);
	}

	@And("Patholythix preclinical logo text is present on the login page")
	public void patholythixPreclinicalLogoIsPresentOnTheLoginPage() {
		Assert.assertTrue(loginPage.isPatholythixPreclinicalLogoDisplayed());
	}

	@And("{string} is present on the login page")
	public void IsPresentOnTheLoginPage(String expectedVersion) {
		String actualVersion = loginPage.getPatholytixPreclinicalVersionText();
		Assert.assertEquals("Expected version " + expectedVersion + " doesn't match, actual " + actualVersion + " version ", expectedVersion, actualVersion);
	}

	@And("Copyright and years interval {string} is present")
	public void copyrightSymbolAndYearsIntervalIsPresent(String expectedCopyrightYearsInterval) {
		String actualCopyrightYearsInterval = loginPage.getCopyRightYearsIntervalText();
		Assert.assertEquals("Expected copyright symbol or interval years " + expectedCopyrightYearsInterval + " doesn't match, actual " + actualCopyrightYearsInterval + " copyright symbol or interval years ", expectedCopyrightYearsInterval, actualCopyrightYearsInterval);
	}

	@When("user clicks on logout button")
	public void userClicksOnLogoutButton() {
		loginPage.clickOnLogoutButton();
	}

	@Given("user is login to case uploader page with user {string}")
	public void userIsLoginToCaseUploaderPageWithUser(String user) throws OAuthException {
		{
			configReader = new ConfigReader();
			prop = configReader.getProperties(Constants.CONFIG_FILE_PATH);
			user_is_on_CaseUploader_page();
			loginPage.doLogin(user, prop.getProperty("password"));

		}
	}

}



