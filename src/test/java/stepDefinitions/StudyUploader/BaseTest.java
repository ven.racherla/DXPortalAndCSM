package stepDefinitions.StudyUploader;

import com.api.StudyAPI;
import com.factory.DriverFactory;
import com.pages.CaseManagement.*;
import com.util.ConfigReader;
import com.util.LoggerHelper;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import java.util.Properties;

public class BaseTest {

	private static final Logger logger = LoggerHelper.getLogger(BaseTest.class);

	public BaseTest(){initPages(DriverFactory.getDriver());}

	protected ConfigReader configReader;
	protected Properties prop;

	protected String title;

	protected BasePage basePage;
	protected CasePage casePage;
	protected LoginPage loginPage;
	protected HomePage homePage;
	protected UploadListPage uploadListPage;
	protected UploadPage upload;
	protected PortalPage portal;
	protected StudyAPI studyAPI = new StudyAPI();

	public void initPages(WebDriver driver){
		basePage = new BasePage(driver);
		PageFactory.initElements(driver, basePage);
		loginPage = new LoginPage(driver);
		PageFactory.initElements(driver, loginPage);
		casePage = new CasePage(driver);
		PageFactory.initElements(driver, casePage);
		homePage = new HomePage(driver);
		PageFactory.initElements(driver, homePage);
		uploadListPage = new UploadListPage(driver);
		PageFactory.initElements(driver, uploadListPage);
		upload = new UploadPage(driver);
		PageFactory.initElements(driver, upload);
		portal = new PortalPage(driver);
		PageFactory.initElements(driver,portal);
	}

	public void getCurrentTitle(WebDriver driver) {
		title = basePage.getCurrentPageTitle();
	}
}

