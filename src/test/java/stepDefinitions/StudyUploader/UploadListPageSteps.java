package stepDefinitions.StudyUploader;

import com.factory.DriverFactory;
import com.util.Constants;
import com.util.Helpers;
import com.util.LoggerHelper;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.apache.log4j.Logger;
import org.junit.Assert;

import java.io.File;
import java.io.IOException;

public class UploadListPageSteps extends BaseTest {

	private static final Logger logger = LoggerHelper.getLogger(UploadListPageSteps.class);

	private static String title;

	@Then("user gets the title of the upload list page")
	public void userGetsTheTitleOfTheUploadListPage() {
		title = uploadListPage.getUploadListPageTitle();
		logger.info("Upload list page title is: " + title);
	}

	@Then("upload list page title should be {string}")
	public void upload_list_page_title_should_be(String expectedTitleName) {
		Assert.assertEquals(title, expectedTitleName);
	}

	@And("username displayed on upload list page is {string}")
	public void usernameDisplayedOnHomepageIs(String expectedUsername) {
		String readUsername = uploadListPage.getUsernameText();
		Assert.assertEquals("Read username on the upload list page doesn't match expected username", readUsername, expectedUsername);
	}

	@Given("user goes to upload list page")
	public void userGoesToUploadListPage() {
		DriverFactory.getDriver()
				.get(Constants.CASE_UPLOADER_BASE_URL.concat(Constants.UPLOAD_LIST_URL));
	}

	@And("user clicks Create new Upload on Upload List page")
	public void userClicksCreateNewUploadOnUploadListPage() {
		uploadListPage.clickCreateNewUpload();
	}

	@Given("user prepares {string} test data {string}")
	public void userPreparesTestData(String featureName, String testDataFolder) throws IOException {
		String directoryPath = (new File("src/test/resources/testData/".concat(featureName).concat("/").concat(testDataFolder)).getAbsolutePath()).toString();
		Helpers.copyDirectory(directoryPath, Constants.STUDY_SOURCE_PATH.concat(testDataFolder));
	}


}
