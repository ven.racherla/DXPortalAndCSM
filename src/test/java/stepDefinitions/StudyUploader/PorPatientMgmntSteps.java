package stepDefinitions.StudyUploader;
import com.factory.DriverFactory;
import com.pages.CaseManagement.CasePage;
import com.util.Constants;
import com.util.Helpers;
import com.util.LoggerHelper;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.time.Duration;

public class PorPatientMgmntSteps extends BaseTest {

    private static final Logger logger = LoggerHelper.getLogger(LoginPageSteps.class);

    private static String title;

    public PorPatientMgmntSteps() throws ClassNotFoundException {

    }
    @Then("User can able to Cancel add Patient")
    public void userableToCanceltheaddpatientinfo() {
        casePage.ClickCancel();

    }

    @When("User Select add button without populating information")
    public void userSelectAddButtonWithoutPopulatingInformation() {
     casePage.ClickAddWithoutpopulatinginfo();
    }

    @Then("user unable to create empty patient")
    public void userUnableToCreateEmptyPatient() {
        casePage.VerifyWarningMessage();
    }

    @When("user expand Patient Information panel.")
    public void userExpandPatientInformationPanel() {
        portal.ExpandPatientPanel();
    }

    @Then("user select edit patient option")
    public void userSelectEditPatientOption() {
         portal.ClickEditPatient();
    }

    @And("user can view patient page")
    public void userCanViewPatientPage() {
         portal.ViewPatient();
    }

    @And("User able to edit patient information")
    public void userAbleToEditPatientInformation() {
        portal.EditPatientInformation();
    }

    @And("user can Delete patient page")
    public void userCanDeletePatientPage() {
    }
}
