package stepDefinitions.StudyUploader;
import com.factory.DriverFactory;
import com.util.Constants;
import com.util.Helpers;
import com.util.LoggerHelper;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.time.Duration;

public class PortalNavigationSteps extends BaseTest {

    private static final Logger logger = LoggerHelper.getLogger(LoginPageSteps.class);

    private static String title;

    public PortalNavigationSteps() throws ClassNotFoundException {
    }

    @And("Verify User is able to view and manage the cases")
    public void userableToViewAndManagetheCases() {
         portal.clickStatus();
         portal.verifyCaseActions();

    }

    @When("user clicks on Home page button")
    public void userClicksOnHomePageButton() {
        portal.clickHome();
    }

    @Then("user able to click Pathologist button")
    public void userAbleToClickPathologistButton() {
        portal.ClickPathologist();
    }

    @And("user able to see Add Pathologist button")
    public void userAbleToSeeAddPathologistButton() {
        portal.AddpathologistisPresent();
    }

    @And("user able to view and manage registered pathologists")
    public void userAbleToViewAndManageRegisteredPathologists() {
       portal.ManagePath();
    }

    @Then("user able to click Patient button")
    public void userAbleToClickPatientButton() {
        portal.ClickPatient();
    }

    @And("user able to see Add Patient button")
    public void userAbleToSeeAddPatientButton() {
        portal.AddPatientisPresent();
    }

    @And("user able to view and manage patients registered to a particular institute")
    public void userAbleToViewAndManagePatientsRegisteredToAParticularInstitute() {
        portal.VerifyPatientActions();
    }

    @Then("user select Event option")
    public void userSelectEventOption() {
        portal.ClickEvent();
    }

    @And("user can see the Event Monitoring")
    public void userCanSeeTheEventMonitoring() {
        portal.VerifyEventActions();
    }

    @When("user clicks on Referring Institute panel")
    public void userClicksOnReferringInstitutePanel() {
         portal.ExpandRefPanel();
    }

    @Then("user clicks on Referring Institute option")
    public void userClicksOnReferringInstituteOption() {
        portal.ClickRefInstitute();
    }

    @Then("user clicks on Delegating Referring Lab option")
    public void userClicksOnDelegatingReferringLabOption() {
        portal.ClickDelegatingReferringLab();
    }

    @And("Verify User is able to view and manage the Ref Inst")
    public void verifyUserIsAbleToViewAndManageTheRefInst() {
        portal.VerifyRefInstActions();
    }

    @And("Verify User is able to view and manage the del Ref Inst")
    public void verifyUserIsAbleToViewAndManageTheDelRefInst() {
        portal.VerifyDelRefLabActions();
    }

    @Then("user clicks on Referring Lab Manager option")
    public void userClicksOnReferringLabManagerOption() {
        portal.ClickReferringLabManager();
    }

    @And("Verify User is able to view and manage the Ref lab manager")
    public void verifyUserIsAbleToViewAndManageTheRefLabManager() {
        portal.VerifyRLMActions();
    }

    @Then("user clicks on Referring Clinician option")
    public void userClicksOnReferringClinicianOption() {
        portal.ClickReferringClinician();
    }

    @And("Verify User is able to view and manage the Referring Clinician")
    public void verifyUserIsAbleToViewAndManageTheReferringClinician() {
        portal.VerifyRCActions();
    }

    @When("user clicks on Diagnexia Laboratory panel")
    public void userClicksOnDiagnexiaLaboratoryPanel() {
        portal.ExpandDiagPanel();
    }

    @Then("user clicks on Clinical Director option")
    public void userClicksOnClinicalDirectorOption() {
        portal.clickClinicalDirector();
    }

    @Then("user clicks on Lab Manager option")
    public void userClicksOnLabManagerOption() {
        portal.clickLabManager();
    }

    @Then("user clicks on Lab option")
    public void userClicksOnLabOption() {
        portal.clickLab();
    }

    @And("Verify Ref User is able to view and manage the cases")
    public void verifyRefUserIsAbleToViewAndManageTheCases() {
        portal.clickStatus();
        portal.verifyRefCaseActions();
    }

    @And("Verify ref User is able to view and manage the del Ref Inst")
    public void verifyRefUserIsAbleToViewAndManageTheDelRefInst() {
      portal.VerifyRLMRLabActions();
    }

    @And("Verify Ref User is able to view and manage the Ref Inst")
    public void verifyRefUserIsAbleToViewAndManageTheRefInst() {
        portal.VerifyRLMRefInstActions();
    }

    @And("Verify ref User is able to view and manage the Ref lab manager")
    public void verifyRefUserIsAbleToViewAndManageTheRefLabManager() {
        portal.VerifyRLMLabMgmtActions();
    }

    @And("Verify ref User is able to view and manage the Referring Clinician")
    public void verifyRefUserIsAbleToViewAndManageTheReferringClinician() {
        portal.VerifyRefCMActions();
    }

    @And("Verify CD is able to view and manage the cases")
    public void verifyCDIsAbleToViewAndManageTheCases() {
        portal.VerifyCDCaseActions();
    }
}


