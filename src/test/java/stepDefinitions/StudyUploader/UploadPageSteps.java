package stepDefinitions.StudyUploader;

import com.deciphex.library.oauth.exception.OAuthException;
import com.jayway.jsonpath.JsonPath;
import com.util.Constants;
import com.util.ExcelReader;
import com.util.Helpers;
import com.util.LoggerHelper;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.io.IOException;
import java.util.*;

public class UploadPageSteps extends BaseTest {

    private static final Logger logger = LoggerHelper.getLogger(UploadListPageSteps.class);

    private static String title;

    @And("user selects study file based on {string} from Upload page")
    public void userSelectsStudyFileBasedOn(String testData) {
        upload.clickOnStudyFileOptionsDropdown();
        upload.selectStudyFileOptionFromDropDownList(testData);
    }

    @And("user chooses {string} from include label dropdown list")
    public void userChoosesNoFromIncludeDropdownList(String selection) {
        upload.clickOnIncludeLabelDropdown();
        upload.selectIncludeLabelFromDropDownList(selection);
    }

    @And("user selects study as a {string} study")
    public void userSelectsStudyAsANonGLPStudy(String selection) {
        upload.clickGLPstudyDropdown();
        upload.selectGLPstudyTypeFromDropDownList(selection);
    }

    @And("user submits new study insertion")
    public void userSubmitsNewStudyInsertion() throws OAuthException {
        //TODO: scroll to see error messages
        upload.insertStudy();
    }

    @Then("{string} data validation message is displayed")
    public void dataValidationMessageIsDisplayed(String message) {
        upload.checkDataValidationMessage(message);
    }

    @When("user opens ERRORS sheet from {string} spreadsheet")
    public void userOpensERRORSSheetFromSpreadsheet() {
            }

    @Then("List of error messages are present on ERROR spreadsheet from {string} spreadsheet")
    public void listOfErrorMessagesArePresentOnErrorSpreadsheetFromSpreadsheet(String testData, DataTable table) throws IOException, InvalidFormatException {
        ExcelReader excelReader = new ExcelReader();
        List<String> listOfExcellFiles = Helpers.getListOfExcellFilesFromDirectory(Constants.STUDY_SOURCE_PATH.concat(testData));
        //11 = ERRORS sheet
        List<Map<String, String>> readExcellData =  excelReader.getData(listOfExcellFiles.get(0), 11);

        List<String> expectedListOfErrors = table.asList();

        List<String> readErrors = new ArrayList<>();
        readExcellData.stream().forEach((readErrorMessage) -> readErrors.add(readErrorMessage.get("Special character '.' not allowed")));
        readErrors.removeIf((element)-> element.isEmpty());
        Assert.assertEquals("Expected list of errors is not equal to list of errors read from ERROR sheet", expectedListOfErrors, readErrors);
    }

    @And("user enters study name based on {string} from Upload page")
    public void userEntersStudyNameBasedOnFromUploadPage(String testData) throws IOException {

        String studyName = upload.getStudyNameBasedOnStudyFileSelectDropdownList(testData);
        upload.insertStudyName(studyName);
    }

    @And("user enters study name {string}")
    public void userEntersStudyName(String studyName) throws IOException, OAuthException {

        //if present, delete study in the system with studyName
        studyIsDeletedFromTheSystem(studyName);

        upload.insertStudyName(studyName);
    }

    @And("{string} study is deleted from the system")
    public void studyIsDeletedFromTheSystem(String studyName) throws OAuthException {

        String studiesAsJSONstring = studyAPI.getStudies().asString();
        String studyId = JsonPath.parse(studiesAsJSONstring).read("$.studies[?(@.studyName== '"+studyName+"')].id").toString();
        if (!studyId.isEmpty() && studyId!=null && studyId.length()>2) {
            studyId = studyId.substring(2,studyId.length()-2);
            studyAPI.deleteStudy(studyId);
            logger.info("Study "+studyName+" was deleted from the system.");
        } else{
            logger.info("No study was deleted. No studyId with "+studyName+" was found");
        }
    }}




