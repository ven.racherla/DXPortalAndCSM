package appHooks;

import com.factory.DriverFactory;
import com.util.*;
import io.cucumber.java.After;
import io.cucumber.java.AfterStep;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import org.apache.log4j.Logger;
import org.openqa.selenium.*;

import java.nio.file.Paths;
import java.util.Properties;

public class ApplicationHooks {

    private static final Logger logger = LoggerHelper.getLogger(ApplicationHooks.class);

    private DriverFactory driverFactory;
    private WebDriver driver;
    private ConfigReader configReader;
    Properties prop;

    @Before(order = 0)
    public void getProperty() {

        configReader = new ConfigReader();
        prop = configReader.getProperties(Constants.CONFIG_FILE_PATH);
    }


    @Before(order = 1)
    public void getHubInstallerJarFromAWS() {
        String hubInstallerDownload = prop.getProperty("hubInstallerDownload");

        if (Boolean.parseBoolean(hubInstallerDownload)) {
            PowerShellRunner powerShellRunner = PowerShellRunner.getInstance();

            logger.info("Executing script to download HUB installer from AWS ... ");
            powerShellRunner.RunPS1Script(String.format(Constants.POWER_SHELL_SCRIPT_PATH, Constants.SCRIPT_DOWNLOAD_HUB_INSTALLER), "120000");
            logger.info("Hub Installer download successfull");
        }
    }

    @Before(order = 2)
    public void getTestDataFromAWS() {
        String downloadTestDataFromAWS = prop.getProperty("downloadTestDataFromAWS");

        if (Boolean.parseBoolean(downloadTestDataFromAWS)) {
            PowerShellRunner powerShellRunner = PowerShellRunner.getInstance();
            logger.info("Executing script to setup test data ... ");
            powerShellRunner.RunPS1Script(String.format(Constants.POWER_SHELL_SCRIPT_PATH, Constants.SCRIPT_SETUP_AWS_TEST_DATA), "300000");
            logger.info("Test data setup from AWS S3 successfull");
        }
    }

    @Before(order = 3)
    public void installHub() throws InterruptedException {
        String newHubInstall = prop.getProperty("installhub");

        if (Boolean.parseBoolean(newHubInstall)) {
            PowerShellRunner powerShellRunner = PowerShellRunner.getInstance();
            powerShellRunner.RunPS1Script(String.format(Constants.POWER_SHELL_SCRIPT_PATH, Constants.SCRIPT_CLEANUP), "15000");
            logger.info("Executing script to install patholythix hub ... ");
            powerShellRunner.RunPS1Script(String.format(Constants.POWER_SHELL_SCRIPT_PATH, Constants.SCRIPT_HUB_INSTALLER), "200000");
            Thread.sleep(10000);
            logger.info("Patholythix Hub install successfull");
        }
    }

    @Before(order = 4)
    public void launchBrowser(Scenario scenario) throws Exception {

        String browserName = prop.getProperty("browser");
        driverFactory = new DriverFactory();
        driver = driverFactory.init_driver(browserName);

        MyScreenRecorder.startRecording(scenario.getName());

    }

    @After(order = 0)
    public void closeBrowser(Scenario scenario) throws Exception {
        if (driver != null) {
            try {
                driver.quit();
                driver = null;
            } catch (NoSuchSessionException | SessionNotCreatedException ex) {
                logger.info("An error occured while closing the driver " + ex);
            }
            MyScreenRecorder.stopRecording();
            String reportPath = "http://localhost:53487/Patholythix_functional-tests/target/test-output/SparkReport/Index.html";
            String recording = Helpers.getLastModified(Paths.get("target/test-output/recordings").toAbsolutePath().toString()).toString().replaceAll(" ", "_").toLowerCase();
            logger.info("Checkout Spark report : " + reportPath);
            logger.info("Checkout scenario " + "[" + scenario.getName() + "] recording");
            logger.info(recording);
        }
    }

    @AfterStep(order = 1)
    public void afterStep(Scenario scenario) {

        // take screenshot:
        String screenshotName = scenario.getName().replaceAll(" ", "_");
        byte[] sourcePath = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
        scenario.attach(sourcePath, "image/png", screenshotName);
        scenario.attach("src/target/test-output/recordings", "avi", scenario.getName());
    }
}
