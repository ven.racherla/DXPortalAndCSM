# Your account access key - must have read access to your S3 Bucket
$accessKey = "AKIAU2OC476MT5FRYVEP"
# Your account secret access key
$secretKey = "GxD9g/+v0L0stVu8yHs+Kgc72RAVHrjB/k5NPhq/"
# The region associated with your bucket e.g. eu-west-1, us-east-1 etc. (see http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/using-regions-availability-zones.html#concepts-regions)
$region = "eu-west-1"
# The name of your S3 Bucket
$bucket = "deciphex"
# The folder in your bucket to copy, including trailing slash. Leave blank to copy the entire bucket
$keyPrefix = "Patholytix Validation/Formal Test Suite Data/Uploader OQ Test Data"
# The local file path where files should be copied
$localPath = "C:\Dechiphex_Hub\s3-download\temp"
$studySourcePath = 'C:\Dechiphex_Hub\StudySource'

Start-Transcript -Path .\Execution.log

if (Test-Path -Path $localPath) {
  Remove-Item -Recurse -Force $localPath
}

if (Test-Path -Path $studySourcePath) {
  Remove-Item -Recurse -Force $studySourcePath
}


$objects = Get-S3Object -BucketName $bucket -KeyPrefix $keyPrefix -AccessKey $accessKey -SecretKey $secretKey

foreach($object in $objects) {
    Write-Output "Downloading test data from AWS S3 ..."
	$localFileName = $object.Key -replace $keyPrefix, ''
	if ($localFileName -ne '') {
		$localFilePath = Join-Path $localPath $localFileName
		Copy-S3Object -BucketName $bucket -Key $object.Key -LocalFile $localFilePath -AccessKey $accessKey -SecretKey $secretKey
	}	
}

Write-Output "Unzipping test data to HUB StudySource location"
Get-ChildItem $localPath -Filter *.zip | Expand-Archive -DestinationPath $studySourcePath -Force

Stop-Transcript