param([switch]$Elevated)

function Test-Admin {
    $currentUser = New-Object Security.Principal.WindowsPrincipal $([Security.Principal.WindowsIdentity]::GetCurrent())
    $currentUser.IsInRole([Security.Principal.WindowsBuiltinRole]::Administrator)
}

if ((Test-Admin) -eq $false)  {
    if ($elevated) {
        # tried to elevate, did not work, aborting
    } else {
        Start-Process powershell.exe -Verb RunAs -ArgumentList ('-noprofile -noexit -file "{0}" -elevated' -f ($myinvocation.MyCommand.Definition))
    }
    exit
}


$deciphexFolder = 'C:\Program Files\Deciphex'
$installFolder = 'C:\Program Files\Deciphex\Patholytix Hub'

Start-Transcript -Path .\cleanUp.log

if (Test-Path -Path $deciphexFolder) {
    cd $installFolder
    .\patholytix-hub.exe stop
    .\patholytix-hub.exe uninstall

    Stop-Process -name patholytix-hub -ErrorAction SilentlyContinue -Force

    cd 'c:\'
    Remove-Item -Recurse -Force $deciphexFolder
}

Stop-Process -name patholytix-hub -ErrorAction SilentlyContinue -Force

Stop-Transcript



