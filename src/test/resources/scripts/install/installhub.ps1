param([switch]$Elevated)

function Test-Admin {
    $currentUser = New-Object Security.Principal.WindowsPrincipal $([Security.Principal.WindowsIdentity]::GetCurrent())
    $currentUser.IsInRole([Security.Principal.WindowsBuiltinRole]::Administrator)
}

if ((Test-Admin) -eq $false)  {
    if ($elevated) {
        # tried to elevate, did not work, aborting
    } else {
        Start-Process powershell.exe -Verb RunAs -ArgumentList ('-noprofile -noexit -file "{0}" -elevated' -f ($myinvocation.MyCommand.Definition))
    }
    exit
}

$installerJarPath = "C:\Dechiphex_Hub\s3-download\hub-installer\PatholytixHub-2.1.2.14-installer.jar"
$installerProperties = "C:\git\Patholythix_functional-tests\src\test\resources\scripts\install\myinstaller.properties"
$installFolder = 'C:\Program Files\Deciphex\Patholytix Hub'
$deciphexFolder = 'C:\Program Files\Deciphex'
$licensKeyFilePath = 'C:\git\Patholythix_functional-tests\src\test\resources\license\mihai-test-dev__8011d7df-ebfd-45ea-9050-83e1e8cd19ee.key'
$configFolder = 'C:\Program Files\Deciphex\Patholytix Hub\config'
$applicationProdProperties = 'C:\Program Files\Deciphex\Patholytix Hub\config\application-prod.properties'
$appendLine = 'deciphex.agent.coreUrl=https://develop.api.deciphex.net'

if (Test-Path -Path $deciphexFolder) {
    cd $installFolder
    .\patholytix-hub.exe stop
    .\patholytix-hub.exe uninstall

    Stop-Process -name patholytix-hub -ErrorAction SilentlyContinue -Force

    cd 'c:\'
    Remove-Item -Recurse -Force $deciphexFolder
}

java -jar $installerJarPath -options $installerProperties

Copy-Item -Path $licensKeyFilePath -Destination $configFolder -PassThru
Add-Content $applicationProdProperties $appendLine

cd $installFolder
.\patholytix-hub.exe start



