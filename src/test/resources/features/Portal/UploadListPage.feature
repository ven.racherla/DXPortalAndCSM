Feature: Upload list page features

Scenario: Upload list page expected web elements are present on the Upload list page

Scenario: Upload list page expected web elements are present on the Upload list page

@all @parallel @uploadList @InDev
Scenario: User can trigger New study upload from Upload list page
  Given user prepares "Upload" test data ""
  And user is logged in with user "mihai.koteles+qa_test_3_0@deciphex.com"
  When  user goes to upload list page
  And user clicks Create new Upload on Upload List page

Scenario: User can check details on the status of study uploads

Scenario: User can search for studies on Upload list page

Scenario: User can navigate through list of studies via pagination on Upload list page

Scenario: User can filter studies by number of entries on Upload list page

Scenario: User can see uploaded study Job in the study uploads status table on Upload-list page

Scenario: User can see uploaded study Status in the study uploads status table on Upload-list page

Scenario: User can see uploaded study Study Name in the study uploads status table on Upload-list page

Scenario: User can see uploaded study Study Spreadsheet in the study uploads status table on Upload-list page

Scenario: User can see uploaded study Message in the study uploads status table on Upload-list page

Scenario: User can see uploaded study Created in the study uploads status table on Upload-list page

Scenario: User can see uploaded study Completed in the study uploads status table on Upload-list page

Scenario: User can see uploaded study Details in the study uploads status table on Upload-list page

Scenario: User can sort entries alphabetically based on Status in the LIS query table on Upload list page

Scenario: User can sort entries alphabetically based on Study Name in the LIS query table on Upload list page

Scenario: User can sort entries based on Job in the status of study uploads table on Upload list page

Scenario: User can sort entries based on Status in the status of study uploads table on Upload list page

Scenario: User can sort entries based on Study Name in the status of study uploads table on Upload list page

Scenario: User can sort entries based on Study Spreadsheet in the status of study uploads table on Upload list page

Scenario: User can sort entries based on Message in the status of study uploads table on Upload list page

Scenario: User can sort entries based on Created in the status of study uploads table on Upload list page

Scenario: User can sort entries based on Completed in the status of study uploads table on Upload list page

Scenario: User can sort entries based on Details in the status of study uploads table on Upload list page

Scenario: User can navigate to Home page from Upload list page

Scenario: User can navigate to Storage page from Upload list page

Scenario: User can navigate to Downloads page from Upload list page

Scenario: User can navigate to Upload list page from Upload list page

Scenario: User can navigate to LIS page from Upload list page

Scenario: User can logout from Upload list page




