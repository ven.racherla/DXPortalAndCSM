@login
Feature: Portal User Navigation features

  Scenario: Lab Manager Can see Case Workflow.
    Given user is logged in with user3 "venkata.racherla+manager@deciphex.com"
    And  user goes to home page
    When user clicks on Case Button
    Then page URL contains "https://portal.dev.diagnexia.com/app/case"
    And  Verify User is able to view and manage the cases
    When user clicks on Home page button
    Then user able to click Pathologist button
    And user able to see Add Pathologist button
    And user able to view and manage registered pathologists
    When user clicks on Home page button
    Then user able to click Patient button
    And user able to see Add Patient button
    And user able to view and manage patients registered to a particular institute
    When user clicks on Home page button
    Then user select Event option
    And  user can see the Event Monitoring

  Scenario:  Lab Manager Can see Referring Institute Workflow
    Given user is logged in with user3 "venkata.racherla+manager@deciphex.com"
    And  user goes to home page
    When user clicks on Referring Institute panel
    Then user clicks on Referring Institute option
    Then page URL contains "https://portal.dev.diagnexia.com/app/institute"
    And  Verify User is able to view and manage the Ref Inst
    When user clicks on Home page button
    When user clicks on Referring Institute panel
    Then user clicks on Delegating Referring Lab option
    And  Verify User is able to view and manage the del Ref Inst
    When user clicks on Home page button
    When user clicks on Referring Institute panel
    Then user clicks on Referring Lab Manager option
    And  Verify User is able to view and manage the Ref lab manager
    When user clicks on Home page button
    When user clicks on Referring Institute panel
    Then user clicks on Referring Clinician option
    And  Verify User is able to view and manage the Referring Clinician

  Scenario: Lab Manager Can see Diagnexia Laboratory Workflow
    Given user is logged in with user3 "venkata.racherla+manager@deciphex.com"
    And  user goes to home page
    When user clicks on Diagnexia Laboratory panel
    Then user clicks on Clinical Director option
    Then page URL contains "https://portal.dev.diagnexia.com/app/director"
    When user clicks on Home page button
    When user clicks on Diagnexia Laboratory panel
    Then user clicks on Lab Manager option
    Then page URL contains "https://portal.dev.diagnexia.com/app/lab-manager"
    When user clicks on Home page button
    When user clicks on Diagnexia Laboratory panel
    Then user clicks on Lab option
    And  page URL contains "https://portal.dev.diagnexia.com/app/lab"

  Scenario: Ref Lab Manager Can see Case Workflow.
    Given user is logged in with user1 "venkata.racherla+ref+manager@deciphex.com"
    And  user goes to home page
    When user clicks on Case Button
    Then page URL contains "https://portal.dev.diagnexia.com/app/case"
    And  Verify Ref User is able to view and manage the cases
    When user clicks on Home page button
    Then user able to click Patient button
    And user able to see Add Patient button
    And user able to view and manage patients registered to a particular institute
    When user clicks on Home page button
    And user clicks on logout button

  Scenario:  Ref Lab Manager Can see Referring Institute Workflow
    Given user is logged in with user1 "venkata.racherla+ref+manager@deciphex.com"
    And  user goes to home page
    When user clicks on Referring Institute panel
    Then user clicks on Referring Institute option
    Then page URL contains "https://portal.dev.diagnexia.com/app/institute"
    And  Verify Ref User is able to view and manage the Ref Inst
    When user clicks on Home page button
    When user clicks on Referring Institute panel
    Then user clicks on Delegating Referring Lab option
    And  Verify ref User is able to view and manage the del Ref Inst
    When user clicks on Home page button
    When user clicks on Referring Institute panel
    Then user clicks on Referring Lab Manager option
    And  Verify ref User is able to view and manage the Ref lab manager
    When user clicks on Home page button
    When user clicks on Referring Institute panel
    Then user clicks on Referring Clinician option
    And  Verify ref User is able to view and manage the Referring Clinician

  Scenario: Clinical Director Can see Case Workflow.
    Given user is logged in with user "venkata.racherla+director@deciphex.com"
    And  user goes to home page
    When user clicks on Case Button
    Then page URL contains "https://portal.dev.diagnexia.com/app/case"
    And  Verify CD is able to view and manage the cases
    When user clicks on Home page button
    Then user able to click Pathologist button
    When user clicks on Home page button
    Then user able to click Patient button
    When user clicks on Home page button
    Then user select Event option
    And  user can see the Event Monitoring

  Scenario: CD Can see Referring Institute Workflow
    Given user is logged in with user "venkata.racherla+director@deciphex.com"
    And  user goes to home page
    When user clicks on Referring Institute panel
    Then user clicks on Referring Institute option
    Then page URL contains "https://portal.dev.diagnexia.com/app/institute"
    When user clicks on Home page button
    When user clicks on Referring Institute panel
    When user clicks on Home page button
    When user clicks on Referring Institute panel
    Then user clicks on Referring Lab Manager option
    When user clicks on Home page button
    When user clicks on Referring Institute panel
    Then user clicks on Referring Clinician option
    And user clicks on logout button


  Scenario: CD Can see Diagnexia Laboratory Workflow
    Given user is logged in with user "venkata.racherla+director@deciphex.com"
    And  user goes to home page
    When user clicks on Diagnexia Laboratory panel
    Then user clicks on Clinical Director option
    Then page URL contains "https://portal.dev.diagnexia.com/app/director"
    When user clicks on Home page button
    When user clicks on Diagnexia Laboratory panel
    Then user clicks on Lab Manager option
    Then page URL contains "https://portal.dev.diagnexia.com/app/lab-manager"
    When user clicks on Home page button
    When user clicks on Diagnexia Laboratory panel
    Then user clicks on Lab option
    And  page URL contains "https://portal.dev.diagnexia.com/app/lab"

  Scenario: Admin Can see Case Workflow.
    Given user is logged in with user2 "venkata.racherla+admin@deciphex.com"
    And  user goes to home page
    When user clicks on Case Button
    Then page URL contains "https://portal.dev.diagnexia.com/app/case"
    And  Verify CD is able to view and manage the cases
    When user clicks on Home page button
    Then user able to click Pathologist button
    When user clicks on Home page button
    Then user able to click Patient button
    When user clicks on Home page button
    Then user select Event option
    And  user can see the Event Monitoring

  Scenario: Admin Can see Referring Institute Workflow
    Given user is logged in with user2 "venkata.racherla+admin@deciphex.com"
    And  user goes to home page
    When user clicks on Referring Institute panel
    Then user clicks on Referring Institute option
    Then page URL contains "https://portal.dev.diagnexia.com/app/institute"
    When user clicks on Home page button
    When user clicks on Referring Institute panel
    When user clicks on Home page button
    When user clicks on Referring Institute panel
    Then user clicks on Referring Lab Manager option
    When user clicks on Home page button
    When user clicks on Referring Institute panel
    Then user clicks on Referring Clinician option
    And user clicks on logout button

  Scenario: Admin Can see Diagnexia Laboratory Workflow
    Given user is logged in with user2 "venkata.racherla+admin@deciphex.com"
    And  user goes to home page
    When user clicks on Diagnexia Laboratory panel
    Then user clicks on Clinical Director option
    Then page URL contains "https://portal.dev.diagnexia.com/app/director"
    When user clicks on Home page button
    When user clicks on Diagnexia Laboratory panel
    Then user clicks on Lab Manager option
    Then page URL contains "https://portal.dev.diagnexia.com/app/lab-manager"
    When user clicks on Home page button
    When user clicks on Diagnexia Laboratory panel
    Then user clicks on Lab option
    And  page URL contains "https://portal.dev.diagnexia.com/app/lab"





