@login
Feature: Portal User Patient Management
  Scenario: Admin user able to perform view and update information on  Patient Management
    Given user is logged in with user2 "venkata.racherla+admin@deciphex.com"
    And user goes to home page
    When user clicks on Case Button
    Then page URL contains "https://portal.dev.diagnexia.com/app/case"
    When user select add case button
    Then User can able to Cancel add Patient
    When user select add case button
    Then User Select add button without populating information
    And  user unable to create empty patient
    When user clicks on Home page button
    Then user able to click Patient button
    Then user select edit patient option
    And  user can view patient page
    When user clicks on Home page button
    Then user able to click Patient button
    Then user select edit patient option
    And User able to edit patient information
    When user clicks on Home page button
    Then user able to click Patient button
    And  user can Delete patient page
