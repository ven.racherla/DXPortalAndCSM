@login
Feature: Login page features

@all @parallel @login
Scenario: Login page expected web elements are present on the Login page
Given user is on login page
  When user gets the title of the login page
  Then login page title should be "Patholytix Hub - Login"
  And Patholythix preclinical logo text is present on the login page
  And "Version 2.1.2.14" is present on the login page
  And Copyright and years interval "© 2020-2021" is present

@all @parallel @login
Scenario: User can login with correct credentials
Given user is on login page
  When user enters username "mihai.koteles+qa_test_3_0@deciphex.com"
  And user enters password "Test123#"
  And user clicks on Login button
  Then user gets the title of the home page
  And page title should be "Patholytix Hub - Home Management - Home"
  And username displayed on home page is "mihai.koteles+qa_test_3_0@deciphex.com"

@all @parallel @login
Scenario: User cannot login with incorrect credentials
Given user is on login page
  When user enters username "invalid@deciphex.com"
  And user enters password "invalid#"
  And user clicks on Login button
  Then Error message is displayed with
    |Error! Login failed, contact your organisation admin. Error code: CORE_AUTH_CTRL_002|
  And "User login failed" is displayed

