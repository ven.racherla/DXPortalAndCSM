Feature: Downloads page features

Scenario: DownloadsList page expected web elements are present on the Upload list page

Scenario: User can see Details of a study downloads

Scenario: User can see Job for a study download on DownloadsList page

Scenario: User can see Study name for a study download on DownloadsList page

Scenario: User can see Status for a study download on DownloadsList page

Scenario: User can see heck Type for a study download on DownloadsList page

Scenario: User can see Total Slides for a study download on DownloadsList page

Scenario: User can see Slides Left for a study download on DownloadsList page

Scenario: User can see ETA for a study download on DownloadsList page

Scenario: User can see Created for a study download on DownloadsList page

Scenario: User can see Completed state for a study download on DownloadsList page

Scenario: User can see Requested by for a study download on DownloadsList page

Scenario: User can see Details by for a study download on DownloadsList page

Scenario: User can see none, one, multiple study download items on DownloadsList page

Scenario: User can search for downloaded studies on DownloadsList page

Scenario: User can choose to hide completed study downloads on DownloadsList page

Scenario: User can choose to show completed study downloads on DownloadsList page

Scenario: User can navigate through list of downloaded studies via pagination on DownloadsList list page

Scenario: User can filter downloaded studies by number of entries in the study downloads table on DownloadsList  page

Scenario: User can sort downloaded studies alphabetically based on Job in the study downloads table on DownloadsList page

Scenario: User can sort downloaded studies alphabetically based on Study Name in the study downloads table on DownloadsList page

Scenario: User can sort downloaded studies alphabetically based on Status in the study downloads table on DownloadsList  list page

Scenario: User can sort downloaded studies alphabetically based on Type in the study downloads table on DownloadsList  list page

Scenario: User can sort downloaded studies based on Total Slides in the study downloads table on DownloadsList  list page

Scenario: User can sort downloaded studies based on Slides Left in the study downloads table on DownloadsList  list page

Scenario: User can sort downloaded studies alphabetically based on ETA in the study downloads table on DownloadsList  list page

Scenario: User can sort downloaded studies based on Created in the study downloads table on DownloadsList  list page

Scenario: User can sort downloaded studies alphabetically based on Completed in the study downloads table on DownloadsList  list page

Scenario: User can sort downloaded studies alphabetically based on Requested By in the study downloads table on DownloadsList  list page

Scenario: User can sort downloaded studies alphabetically based on Details in the study downloads table on DownloadsList  list page

Scenario: User can navigate to Home page from DownloadsList page

Scenario: User can navigate to Storage page from DownloadsList page

Scenario: User can navigate to Downloads page from DownloadsList page

Scenario: User can navigate to Upload list page from DownloadsList page

Scenario: User can navigate to Upload page from DownloadsList page

Scenario: User can logout from Downloads page


