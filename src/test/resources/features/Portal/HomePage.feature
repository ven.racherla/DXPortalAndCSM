Feature: Homepage features

  @all @parallel @homepage
  Scenario Outline: User can see Case Management Page.
    Given user is logged in with user <user>
    And user goes to home page
    When user clicks on Case Button
    Then page URL contains <value>
    When user select status to unassigned
    Then user assign a case to Pathologist
    And user clicks on logout button
    Examples:
      | user                                     | value                                       |
      | "venkata.racherla+director@deciphex.com" | "https://portal.dev.diagnexia.com/app/case" |


  @all @parallel @homepage
  Scenario: Lab User can see Case Management Page.
    Given user is logged in with user1 "venkata.racherla+ref+manager@deciphex.com"
    And user goes to home page
    When user clicks on Case Button
    Then page URL contains "https://portal.dev.diagnexia.com/app/case"
    When user select add case button
    Then user submits case information
    And user clicks on logout button

  Scenario: admin can Logout from Home page
    Given user is logged in with user2 "venkata.racherla+admin@deciphex.com"
    And user goes to home page
    When user clicks on logout button
    Then user goes to home page


  Scenario: Manager can Logout from Home page
    Given user is logged in with user3 "venkata.racherla+manager@deciphex.com"
    And user goes to home page
    When user clicks on logout button
    Then user goes to home page

  Scenario: Clinical User can Logout from Home page
    Given user is logged in with user4 "venkata.racherla+clinician@deciphex.com"
    And user goes to home page
    When user clicks on logout button
    Then user goes to home page


  Scenario Outline: User can Navigates to case file update page from Home page
    Given user is login to case uploader page with user <user>
    When user clicks on add files option from case uploader page.
    Then user gets the title of the upload list page
    And upload list page title should be <expectedTitleName>
    And Content displayed on case page is <expectedActiveContent>
    Examples:
      | user                                        | expectedTitleName                             | expectedActiveContent                         |
      | "venkata.racherla+ref+manager@deciphex.com" | "Lab Manager Ref. Lab Manager @ Diagnexia Case Uploader > Update" | "Home Accession Add Files (current) Extra Material Portal Uploads Logout" |

  Scenario Outline: User can Navigates to Portal Page from Case Management
    Given user is login to case uploader page with user <user>
    When user clicks on add files option from case uploader page.
    Then user gets the title of the upload list page
    And User click on portal
    And Home page title should be <expectedTitleName>
    Examples:
      | user                                        | expectedTitleName                             |
      | "venkata.racherla+ref+manager@deciphex.com" | "Lab Manager Ref. Lab Manager @ Diagnexia Case Uploader > Update" |



