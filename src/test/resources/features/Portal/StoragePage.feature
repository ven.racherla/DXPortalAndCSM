Feature: Storage page features

Scenario: Storage page expected web elements are present on the Upload list page

Scenario: User can initiate a new download on Storage page

Scenario: User can search for studies on Storage page

Scenario: User can navigate through list of studies via pagination on Storage list page

Scenario: User can filter studies by number of entries on Storage list page

Scenario: User can see Study name stored in cache in the studies storage details table on Cache-status page

Scenario: User can see study Status for a study stored in cache in the Studies storage details table on Cache-status page

Scenario: User can see study Status for a study stored in cache in the Studies storage details table on Cache-status page

Scenario: User can see study Size for a study stored in cache in the Studies storage details table on Cache-status page

Scenario: User can see study number of Slides for a study stored in cache in the Studies storage details table on Cache-status page

Scenario: User can see study Remaining size to be downloaded for a study stored in cache in the Studies storage details table on Cache-status page

Scenario: User can see study Last Accessed date and time for a study stored in cache in the Studies storage details table on Cache-status page

Scenario: User can see study Last Accessed by for a study stored in cache  in the Studies storage details table on Cache-status page

Scenario: User can see study Action for a study stored in cache  in the Studies storage details table on Cache-status page

Scenario: User can sort studies alphabetically based on Study in the Studies table on Storage list page

Scenario: User can sort studies alphabetically based on Status in the Studies table on Storage list page

Scenario: User can sort studies based on Size in the Studies table on Storage list page

Scenario: User can sort studies based on Remaining in the Studies table on Storage list page

Scenario: User can sort studies based on Last Accessed in the Studies table on Storage list page

Scenario: User can sort studies based on Last Accessed by in the Studies table on Storage list page

Scenario: User can sort studies based on Action in the Studies table on Storage list page

Scenario: User can save cache size and maximum utilisation threshold on Storage page

Scenario: User can check Auto-eviction on Storage page

Scenario: User can evict studies by number of entries on Storage list page

Scenario: User can lock studies against eviction on Storage list page

Scenario: User can navigate to Home page from Storage page

Scenario: User can navigate to Storage page from Storage page

Scenario: User can navigate to Downloads page from Storage page

Scenario: User can navigate to Upload list page from Storage page

Scenario: User can navigate to LIS page from Storage page

Scenario: User can logout from Storage page




