Feature: List list page features

Scenario: LisList page expected web elements are present on the Upload list page

Scenario: User can see Details for a LIS query on LIsList page

Scenario: User can see Study Name for a LIS query on LIsList page

Scenario: User can see Script for a LIS query on LIsList page

Scenario: User can see Config for a LIS query on LIsList page

Scenario: User can see Message for a LIS query on LIsList page

Scenario: User can see Start for a LIS query on LIsList page

Scenario: User can see Details for a LIS query on LIsList page

Scenario: User can sort LIS queries based on Status in the LIS queries status table on LIsList page

Scenario: User can sort LIS queries based on Study Name in the LIS queries status table on LIsList page

Scenario: User can sort LIS queries based on Script in the LIS queries status table on LIsList page

Scenario: User can sort LIS queries based on Config in the LIS queries status table on LIsList page

Scenario: User can sort LIS queries based on Message in the LIS queries status table on LIsList page

Scenario: User can sort LIS queries based on Start in the LIS queries status table on LIsList page

Scenario: User can sort LIS queries based on Details in the LIS queries status table on LIsList page

Scenario: User can navigate to Home page from  LisList   page

Scenario: User can navigate to Storage page from  LisList   page

Scenario: User can navigate to Downloads page from  LisList   page

Scenario: User can navigate to Upload list page from  LisList   page

Scenario: User can navigate to LIS page from  LisList   page



