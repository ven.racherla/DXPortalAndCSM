Feature: Upload study page features

  Scenario: Upload page expected web elements are present on the Upload list page

  @all
  @parallel
  @upload
  @OQ_SU_TC2_07 #missing slide
  @OQ_SU_TC2_08 #corrupted slide .svs format
  @OQ_SU_TC2_09 #corrupted or unsupported image formats .jpg, .png .PNG
  @OQ_SU_TC2_15 #invalid column name containing a dot "."
  Scenario: User cannot submit new non-GLP study upload from Upload page using invalid data
    Given user prepares "Upload" test data "upload_negative"
    And user is logged in with user "mihai.koteles+qa_test_3_0@deciphex.com"
    When user goes to upload list page
    And user clicks Create new Upload on Upload List page
    And user enters study name based on "upload_negative" from Upload page
    And user selects study file based on "upload_negative" from Upload page
    And user chooses "Yes" from include label dropdown list
    And user selects study as a "non-GLP" study
    And user submits new study insertion
    Then "Check errors on Spreadsheet." data validation message is displayed
    And List of error messages are present on ERROR spreadsheet from "upload_negative" spreadsheet
      | (70078_none.svs) not found                                         |
      | Invalid Sheet(SLIDE) at Row(3), Image is Null/Blank                |
      | (corrupted_file.svs) is either corrupted or not a supported format |
      | (Study8_1.jpg) not found                                           |
      | (Study8_2.png) not found                                           |
      | (Study8_3.PNG) not found                                           |
      | (70078_0) is duplicated                                            |

  @all
  @parallel
  @upload
  @OQ_SU_TC2_14 #exceeding the max number of columns (max=200) in the input spreadsheet
  @OQ_SU_TC2_17 #using the legacy spreadsheet: SEND format - “Unknown workbook type”
  @OQ_SU_TC2_18 #using the legacy spreadsheet: SIMPLE format - “Unknown workbook type”
  @OQ_SU_TC2_19 #using the legacy spreadsheet: FLAT format - “Unknown workbook type”
  Scenario Outline: User cannot submit new non-GLP study upload from Upload page using legacy spreadsheets or by exceeding max number of columns
    Given user prepares "Upload" test data "<testData>"
    And user is logged in with user "mihai.koteles+qa_test_3_0@deciphex.com"
    When user goes to upload list page
    And user clicks Create new Upload on Upload List page
    And user enters study name based on "<testData>" from Upload page
    And user selects study file based on "<testData>" from Upload page
    And user chooses "Yes" from include label dropdown list
    And user selects study as a "non-GLP" study
    And user submits new study insertion
    Then "<errorMessage>" data validation message is displayed
    Examples:
      | testData                             | errorMessage                                                 |
      | upload_negative_legacy_SIMPLE_format | Unknown workbook type                                        |
      | upload_negative_legacy_SEND_format   | Unknown workbook type                                        |
      | upload_negative_legacy_FLAT_format   | Unknown workbook type                                        |
      | upload_negative_too_many_columns     | Too many columns in the study (max number of columns is 200) |

#  @OQ_SU_TC2_16 #exisitng study - “StudyName already exists in the cloud, please choose another

  Scenario: User can submit new GLP study upload from Upload page
    Given user prepares "Upload" test data "upload_positive"
    And user is logged in with user "mihai.koteles+qa_test_3_0@deciphex.com"
    When user goes to upload list page
    And user clicks Create new Upload on Upload List page
    And user enters study name "Universal_Flat_1"
    And user selects study file based on "upload_positive" from Upload page
    And user chooses "Yes" from include label dropdown list
    And user selects study as a "non-GLP" study
    And user submits new study insertion
#    Then user waits for status "COMPLETED" for study name "Universal_Flat_1" on Upload List table
#    When user clicks on "DETAILS" button for "Universal_Flat_1" uploaded study
#    Then user can see there is "1" entry in the upload status table
#    And user can see image upload status is "UPLOADED"
#    And user can see upload image name is "70078.svs"
#    And user can see upload message is "Upload completed successfully
#    And "Universal_Flat_1" study is deleted from the system


  Scenario: User can submit update of an existing study from Upload page
    And "Universal_Flat_1" study is deleted from the system

  Scenario: User can submit export existing study from Upload page

  Scenario: User can navigate to Home page from Upload page

  Scenario: User can navigate to Storage page from Upload page

  Scenario: User can navigate to Downloads page from Upload page

  Scenario: User can navigate to Upload list page from Upload page

  Scenario: User can navigate to Upload page from LIS page

  Scenario: User can logout from Upload page







